#include "catch.hpp"

#include "types.h"

using namespace idun;

TEST_CASE("Color inversion", "[types]")
{
    Color w = Color::WHITE;
    Color b = Color::BLACK;

    REQUIRE(w != b);
    REQUIRE(w == invert(b));
    REQUIRE(invert(w) == b);
    REQUIRE(invert(w) != invert(b));
}
