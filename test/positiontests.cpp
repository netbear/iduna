#include "catch.hpp"

#include "position.h"

#include <stack>

using namespace idun;

TEST_CASE("Position comparison is correct", "[position]")
{
    Position empty;
    Position initial(
        "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
    Position initialBlackToMove(
        "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR b KQkq - 0 1");
    Position e4("rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1");
    Position e4b("rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e5 0 1");

    REQUIRE(empty == empty);
    REQUIRE(empty != initial);
    REQUIRE(empty != e4);

    REQUIRE(initial == initial);
    REQUIRE(initial != e4);

    REQUIRE(initialBlackToMove == initialBlackToMove);
    REQUIRE(initial != initialBlackToMove);

    REQUIRE(e4 != e4b);
}

TEST_CASE("Fen parsing works correctly", "[position, fen]")
{
    std::string fen =
        "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR b Qq h3 3 42";
    Position p(fen);

    REQUIRE(p.activeColor() == Color::BLACK);

    auto castling =
        CastlingRights::BLACK_QUEEN_SIDE | CastlingRights::WHITE_QUEEN_SIDE;
    REQUIRE(p.getCastlingRights() == castling);
    REQUIRE(p.getEnPassantSquare() == Square(16));
    REQUIRE(p.getHalfMoveClock() == 3);
    REQUIRE(p.getFullMoveNumber() == 42);
    REQUIRE(p.fen() == fen);

    std::string fen2 =
        "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w Kk a8 0 1";
    Position p2(fen2);
    REQUIRE(p2.activeColor() == Color::WHITE);
    auto castling2 =
        CastlingRights::BLACK_KING_SIDE | CastlingRights::WHITE_KING_SIDE;
    REQUIRE(p2.getCastlingRights() == castling2);
    REQUIRE(p2.getEnPassantSquare() == Square(63));
    REQUIRE(p2.getHalfMoveClock() == 0);
    REQUIRE(p2.getFullMoveNumber() == 1);
    REQUIRE(p2.fen() == fen2);
}

TEST_CASE("Fen generation works correctly", "[position, fen]")
{
    std::string fen = "rnbqkbnr/pp3p2/8/8/8/8/PP1PPPPP/7R w Kq e4 0 1";
    Position p(fen);
    REQUIRE(p.fen() == fen);
}

TEST_CASE("Normal moves works correctly", "[position, move]")
{
    std::stack<MoveInformation> mis;
    MoveInformation mi;

    Position p("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");

    p.doMove(Move(Square(11), Square(27)), &mi);
    mis.push(mi);
    p.doMove(Move(Square(62), Square(45)), &mi);
    mis.push(mi);

    REQUIRE(p.fen() ==
            "r1bqkbnr/pppppppp/2n5/8/4P3/8/PPPP1PPP/RNBQKBNR w KQkq - 1 2");

    while(!mis.empty())
    {
        mi = mis.top();
        p.undoMove(&mi);
        mis.pop();
    }

    REQUIRE(p.fen() ==
            "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
}

TEST_CASE("En passant capture move", "[position, move]")
{
    MoveInformation mi;
    Position p("rnbqkbnr/1pppp1pp/p7/4Pp2/8/8/PPPP1PPP/RNBQKBNR w KQkq f6 0 3");

    p.doMove(Move(Square(35), Square(42), Move::Type::EN_PASSANT), &mi);
    REQUIRE(p.fen() ==
            "rnbqkbnr/1pppp1pp/p4P2/8/8/8/PPPP1PPP/RNBQKBNR b KQkq - 0 3");

    p.undoMove(&mi);
    REQUIRE(p.fen() ==
            "rnbqkbnr/1pppp1pp/p7/4Pp2/8/8/PPPP1PPP/RNBQKBNR w KQkq f6 0 3");
}

TEST_CASE("Castling move is executed correctly", "[position, move]")
{
    SECTION("White king side")
    {
        Position p("r1bqkb1r/pppp1ppp/2n2n2/1B2p3/4P3/5N2/PPPP1PPP/RNBQK2R w "
                   "KQkq - 4 4");
        MoveInformation mi;
        p.doMove(Move(Square(3), Square(1), Move::Type::CASTLING), &mi);
        REQUIRE(p.fen() == "r1bqkb1r/pppp1ppp/2n2n2/1B2p3/4P3/5N2/PPPP1PPP/"
                           "RNBQ1RK1 b kq - 5 4");
        p.undoMove(&mi);
        REQUIRE(p.fen() == "r1bqkb1r/pppp1ppp/2n2n2/1B2p3/4P3/5N2/PPPP1PPP/"
                           "RNBQK2R w KQkq - 4 4");
    }

    SECTION("White queen side")
    {
        Position p(
            "rnbqkbnr/pp3p1p/3p4/2p1p2p/3PPB2/2N5/PPP2PPP/R3KBNR w KQkq - 0 6");
        MoveInformation mi;
        p.doMove(Move(Square(3), Square(5), Move::Type::CASTLING), &mi);
        REQUIRE(
            p.fen() ==
            "rnbqkbnr/pp3p1p/3p4/2p1p2p/3PPB2/2N5/PPP2PPP/2KR1BNR b kq - 1 6");
        p.undoMove(&mi);
        REQUIRE(
            p.fen() ==
            "rnbqkbnr/pp3p1p/3p4/2p1p2p/3PPB2/2N5/PPP2PPP/R3KBNR w KQkq - 0 6");
    }

    SECTION("Black king side")
    {
        Position p("rnbqk2r/pppp1ppp/3b1n2/4p3/2B1P3/3P1N2/PPP2PPP/RNBQK2R b "
                   "KQkq - 0 4");
        MoveInformation mi;
        p.doMove(Move(Square(59), Square(57), Move::Type::CASTLING), &mi);
        REQUIRE(p.fen() == "rnbq1rk1/pppp1ppp/3b1n2/4p3/2B1P3/3P1N2/PPP2PPP/"
                           "RNBQK2R w KQ - 1 5");
        p.undoMove(&mi);
        REQUIRE(p.fen() == "rnbqk2r/pppp1ppp/3b1n2/4p3/2B1P3/3P1N2/PPP2PPP/"
                           "RNBQK2R b KQkq - 0 4");
    }

    SECTION("Black queen side")
    {
        Position p("r3kbnr/pbpp1ppp/1pn2q2/4p1B1/2P1P3/3P1P2/PP2B1PP/RN1QK1NR "
                   "b KQkq c3 0 6");
        MoveInformation mi;
        p.doMove(Move(Square(59), Square(61), Move::Type::CASTLING), &mi);
        REQUIRE(p.fen() == "2kr1bnr/pbpp1ppp/1pn2q2/4p1B1/2P1P3/3P1P2/PP2B1PP/"
                           "RN1QK1NR w KQ - 1 7");
        p.undoMove(&mi);
        REQUIRE(p.fen() == "r3kbnr/pbpp1ppp/1pn2q2/4p1B1/2P1P3/3P1P2/PP2B1PP/"
                           "RN1QK1NR b KQkq c3 0 6");
    }
}
