#include "catch.hpp"

#include "timemanager.h"

using namespace idun;

TEST_CASE("Time manager does not schedule longer", "[timemanager]")
{
    TimeManager tm;

    SearchOptions so;
    so.setWhiteTimeLeft(60000);
    so.setWhiteIncrement(0);

    SECTION("No moves to go.")
    {
        tm.init(so, Color::WHITE, 10);
        REQUIRE(tm.time() < so.whiteTimeLeft());
    }

    SECTION("5 move to go.")
    {
        so.setMovesToGo(5);
        tm.init(so, Color::WHITE, 10);
        REQUIRE(tm.time() < so.whiteTimeLeft());
    }
}
