#include "catch.hpp"

#include "mathutils.h"

using namespace idun;

TEST_CASE("Sign", "[math]")
{
    REQUIRE(sign(1337) == 1);
    REQUIRE(sign(-1234) == -1);
    REQUIRE(sign(0) == 0);
}

TEST_CASE("Safe symetric addition", "[math]")
{
    REQUIRE(safeSymetricAddition(100, 100) == 200);
    REQUIRE(safeSymetricAddition(std::numeric_limits<decltype(100)>::max() - 50,
                                 100) ==
            std::numeric_limits<decltype(100)>::max());
    REQUIRE(safeSymetricAddition(
                -std::numeric_limits<decltype(100)>::max() + 50, -100) ==
            -std::numeric_limits<decltype(100)>::max());
}

TEST_CASE("Safe symetric substraction", "[math]")
{
    REQUIRE(safeSymetricSubstraction(100, 100) == 0);
    REQUIRE(safeSymetricSubstraction(
                std::numeric_limits<decltype(1)>::max() - 50, -100) ==
            std::numeric_limits<decltype(1)>::max());
    REQUIRE(safeSymetricSubstraction(
                -std::numeric_limits<decltype(1)>::max() + 50, 100) ==
            -std::numeric_limits<decltype(1)>::max());
}
