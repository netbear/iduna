#include "catch.hpp"

#include "move.h"

using namespace idun;

TEST_CASE("Move representation is correct", "[move]")
{
    REQUIRE(Move().isValid() == false);
    REQUIRE(Move(Square(42), Square(42)).isValid() == false);
    REQUIRE(sizeof(Move) == 2);

    Move move(Square(42), Square(43));
    REQUIRE(move.isValid() == true);
    REQUIRE(move.origin() == Square(42));
    REQUIRE(move.destination() == Square(43));
    REQUIRE(move.promotionType() == PieceType::KNIGHT);
    REQUIRE(move.type() == Move::Type::NORMAL);

    Move move2(Square(0), Square(63), Move::Type::EN_PASSANT, PieceType::QUEEN);
    REQUIRE(move2.isValid() == true);
    REQUIRE(move2.origin() == Square(0));
    REQUIRE(move2.destination() == Square(63));
    REQUIRE(move2.promotionType() == PieceType::QUEEN);
    REQUIRE(move2.type() == Move::Type::EN_PASSANT);
}

TEST_CASE("Move comparison is correct", "[move]")
{
    Move empty;
    Move empty2;
    REQUIRE(empty == empty2);

    Move a1(Square(1), Square(2));
    Move a2(Square(1), Square(2));
    Move b1(Square(1), Square(3));
    Move c1(Square(1), Square(3), Move::Type::PROMOTION, PieceType::ROOK);
    Move c2(Square(1), Square(3), Move::Type::PROMOTION, PieceType::ROOK);

    REQUIRE(a1 == a2);
    REQUIRE(a1 != b1);
    REQUIRE(c1 == c2);
}
