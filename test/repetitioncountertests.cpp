#include "catch.hpp"

#include "repetitioncounter.h"

#include <stack>

using namespace idun;

TEST_CASE("Repetition counter counts correct", "[repetitioncounter]")
{
    RepetitionCounter rc;

    rc.push(42);
    rc.push(43);

    rc.push(44);
    REQUIRE(rc.repetitionCount() == 1);
    rc.push(42);

    REQUIRE(rc.repetitionCount() == 1);
    rc.push(42);
    REQUIRE(rc.repetitionCount() == 2);
    rc.push(43);

    rc.push(42);
    REQUIRE(rc.repetitionCount() == 3);
}
