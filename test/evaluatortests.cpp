#include <catch.hpp>

#include "evaluator.h"

TEST_CASE("test evaluation symetry", "[evaluation]")
{
    idun::Evaluator eval;

    SECTION("initial position")
    {
        idun::Position p;
        p.set("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
        idun::Score s = eval.evaluate(p);
        REQUIRE(s == 0);
    }

    SECTION("pawns")
    {
        idun::Position p;
        p.set("pppppppp/pppppppp/pppppppp/pppppppp/PPPPPPPP/PPPPPPPP/PPPPPPPP/"
              "PPPPPPPP w KQkq - 0 1");
        idun::Score s = eval.evaluate(p);
        REQUIRE(s == 0);
    }

    SECTION("knights")
    {
        idun::Position p;
        p.set("nnnnnnnn/nnnnnnnn/nnnnnnnn/nnnnnnnn/NNNNNNNN/NNNNNNNN/NNNNNNNN/"
              "NNNNNNNN w KQkq - 0 1");
        idun::Score s = eval.evaluate(p);
        REQUIRE(s == 0);
    }

    SECTION("bishops")
    {
        idun::Position p;
        p.set("bbbbbbbb/bbbbbbbb/bbbbbbbb/bbbbbbbb/BBBBBBBB/BBBBBBBB/BBBBBBBB/"
              "BBBBBBBB w KQkq - 0 1");
        idun::Score s = eval.evaluate(p);
        REQUIRE(s == 0);
    }

    SECTION("rooks")
    {
        idun::Position p;
        p.set("rrrrrrrr/rrrrrrrr/rrrrrrrr/rrrrrrrr/RRRRRRRR/RRRRRRRR/RRRRRRRR/"
              "RRRRRRRR w KQkq - 0 1");
        idun::Score s = eval.evaluate(p);
        REQUIRE(s == 0);
    }

    SECTION("queens")
    {
        idun::Position p;
        p.set("qqqqqqqq/qqqqqqqq/qqqqqqqq/qqqqqqqq/QQQQQQQQ/QQQQQQQQ/QQQQQQQQ/"
              "QQQQQQQQ w KQkq - 0 1");
        idun::Score s = eval.evaluate(p);
        REQUIRE(s == 0);
    }

    SECTION("kings")
    {
        idun::Position p;
        p.set("kkkkkkkk/kkkkkkkk/kkkkkkkk/kkkkkkkk/KKKKKKKK/KKKKKKKK/KKKKKKKK/"
              "KKKKKKKK w KQkq - 0 1");
        idun::Score s = eval.evaluate(p);
        REQUIRE(s == 0);
    }
}

TEST_CASE("test piece value", "[evaluation]")
{
    idun::Evaluator eval;

    SECTION("king > queen")
    {
        idun::Position p;
        p.set("q7/8/8/8/8/8/8/7K w KQkq - 0 1");
        idun::Score s = eval.evaluate(p);
        REQUIRE(s > 0);
    }

    SECTION("queen > rook")
    {
        idun::Position p;
        p.set("r7/8/8/8/8/8/8/7Q w KQkq - 0 1");
        idun::Score s = eval.evaluate(p);
        REQUIRE(s > 0);
    }

    SECTION("rook > bishop")
    {
        idun::Position p;
        p.set("b7/8/8/8/8/8/8/7R w KQkq - 0 1");
        idun::Score s = eval.evaluate(p);
        REQUIRE(s > 0);
    }

    SECTION("bishop >= knight")
    {
        idun::Position p;
        p.set("n7/8/8/8/8/8/8/7B w KQkq - 0 1");
        idun::Score s = eval.evaluate(p);
        REQUIRE(s >= 0);
    }

    SECTION("knight > pawn")
    {
        idun::Position p;
        p.set("p7/8/8/8/8/8/8/7N w KQkq - 0 1");
        idun::Score s = eval.evaluate(p);
        REQUIRE(s > 0);
    }
}
