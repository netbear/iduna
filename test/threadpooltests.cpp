#include "threadpool.h"
#include <catch.hpp>

TEST_CASE("Test thread pool", "[threadpool]")
{
    idun::ThreadPool pool;
    pool.start();

    std::vector<boost::future<int>> futures;
    std::atomic<int> count(0);
    for(int i = 0; i < 16; ++i)
    {
        auto f = pool.enqueue([i, &count]() {
            ++count;
            return i;
        });
        futures.push_back(std::move(f));
    }

    boost::wait_for_all(futures.begin(), futures.end());

    REQUIRE(count == 16);

    pool.stop();
}
