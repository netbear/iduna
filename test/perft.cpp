#include "catch.hpp"

#include "move.h"
#include "movegenerator.h"

#include "perftsuite.h"

#include <boost/tokenizer.hpp>
#include <sstream>

using namespace idun;

unsigned long long perft(Position position, MoveGenerator &moveGenerator,
                         int depth, bool divide = false)
{
    if(depth == 0)
    {
        return 1;
    }

    unsigned long long nodes = 0;
    unsigned long long movesCount = 0;

    auto moves = moveGenerator.generateLegalMoves(position);
    for(auto move : moves)
    {
        MoveInformation mi;
        position.doMove(move, &mi);

        unsigned long long n = perft(position, moveGenerator, depth - 1, false);

        if(divide)
        {
            std::cout << move << " " << n << " " << position.fen() << std::endl;
        }

        position.undoMove(&mi);

        ++movesCount;
        nodes += n;
    }

    if(divide)
    {
        std::cout << "Moves: " << movesCount << std::endl;
        std::cout << "Nodes: " << nodes << std::endl;
    }

    return nodes;
}

TEST_CASE("Perft tests", "[perft]")
{
    MoveGenerator mg;
    Position p;

    // Test from "TalkChess PERFT Tests (by Martin Sedlak)"
    SECTION("Illegal ep move #1")
    {
        p.set("3k4/3p4/8/K1P4r/8/8/8/8 b - - 0 1");
        REQUIRE(perft(p, mg, 6) == 1134888);
    }

    SECTION("Illegal ep move #2")
    {
        p.set("8/8/4k3/8/2p5/8/B2P2K1/8 w - - 0 1");
        REQUIRE(perft(p, mg, 6) == 1015133);
    }

    SECTION("EP Capture Checks Opponent")
    {
        p.set("8/8/1k6/2b5/2pP4/8/5K2/8 b - d3 0 1");
        REQUIRE(perft(p, mg, 6) == 1440467);
    }

    SECTION("Short Castling Gives Check")
    {
        p.set("5k2/8/8/8/8/8/8/4K2R w K - 0 1");
        REQUIRE(perft(p, mg, 6) == 661072);
    }

    SECTION("Long Castling Gives Check")
    {
        p.set("3k4/8/8/8/8/8/8/R3K3 w Q - 0 1");
        REQUIRE(perft(p, mg, 6) == 803711);
    }

    SECTION("Castle Rights")
    {
        p.set("r3k2r/1b4bq/8/8/8/8/7B/R3K2R w KQkq - 0 1");
        REQUIRE(perft(p, mg, 4) == 1274206);
    }

    SECTION("Castling Prevented")
    {
        p.set("r3k2r/8/3Q4/8/8/5q2/8/R3K2R b KQkq - 0 1");
        REQUIRE(perft(p, mg, 4) == 1720476);
    }

    SECTION("Promote out of Check")
    {
        p.set("2K2r2/4P3/8/8/8/8/8/3k4 w - - 0 1");
        REQUIRE(perft(p, mg, 6) == 3821001);
    }

    SECTION("Discovered Check")
    {
        p.set("8/8/1P2K3/8/2n5/1q6/8/5k2 b - - 0 1");
        REQUIRE(perft(p, mg, 5) == 1004658);
    }

    SECTION("Promote to give check")
    {
        p.set("4k3/1P6/8/8/8/8/K7/8 w - - 0 1");
        REQUIRE(perft(p, mg, 6) == 217342);
    }

    SECTION("Under Promote to give check")
    {
        p.set("8/P1k5/K7/8/8/8/8/8 w - - 0 1");
        REQUIRE(perft(p, mg, 6) == 92683);
    }

    SECTION("Self Stalemate")
    {
        p.set("K1k5/8/P7/8/8/8/8/8 w - - 0 1");
        REQUIRE(perft(p, mg, 6) == 2217);
    }

    SECTION("Stalemate & Checkmate")
    {
        p.set("8/k1P5/8/1K6/8/8/8/8 w - - 0 1");
        REQUIRE(perft(p, mg, 7) == 567584);
    }

    SECTION("Stalemate & Checkmate")
    {
        p.set("8/8/2k5/5q2/5n2/8/5K2/8 b - - 0 1");
        REQUIRE(perft(p, mg, 4) == 23527);
    }
}

TEST_CASE("Long running perft tests", "[.][perft]")
{
    MoveGenerator mg;
    Position p;

    // Test from "http://chessprogramming.wikispaces.com/Perft+Results"
    SECTION("Initial position")
    {
        // rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1; perft 6 =
        // 119060324
        p.set("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
        REQUIRE(perft(p, mg, 0) == 1);
        REQUIRE(perft(p, mg, 1) == 20);
        REQUIRE(perft(p, mg, 2) == 400);
        REQUIRE(perft(p, mg, 3) == 8902);
        REQUIRE(perft(p, mg, 4) == 197281);
        REQUIRE(perft(p, mg, 5) == 4865609);
        REQUIRE(perft(p, mg, 6) == 119060324);
    }

    SECTION("Test 1")
    {
        // r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq -;
        // perft 5 = 193690690
        p.set("r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq "
              "-  0 1");
        REQUIRE(perft(p, mg, 5) == 193690690);
    }

    SECTION("Test 2")
    {
        // 8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - -; perft 7 = 178633661
        p.set("8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - - 0 1");
        REQUIRE(perft(p, mg, 7) == 178633661);
    }

    SECTION("Test 3")
    {
        // r3k2r/Pppp1ppp/1b3nbN/nP6/BBP1P3/q4N2/Pp1P2PP/R2Q1RK1 w kq - 0 1;
        // perft 6 = 706045033
        p.set(
            "r3k2r/Pppp1ppp/1b3nbN/nP6/BBP1P3/q4N2/Pp1P2PP/R2Q1RK1 w kq - 0 1");
        REQUIRE(perft(p, mg, 6) == 706045033);
    }

    SECTION("Test 4")
    {
        // 1k6/1b6/8/8/7R/8/8/4K2R b K - 0 1; perft 5 = 1063513
        p.set("1k6/1b6/8/8/7R/8/8/4K2R b K - 0 1");
        REQUIRE(perft(p, mg, 5) == 1063513);
    }
}

TEST_CASE("Extensive perft test suite", "[.][perft][perftsuite]")
{
    MoveGenerator mg;
    Position p;

    for(const char *line : perftTestSuite)
    {
        std::string testCase(line);
        boost::tokenizer<boost::char_separator<char>> tokenizer(
            testCase, boost::char_separator<char>(";"));

        auto token = tokenizer.begin();
        std::string fen = *token;

        for(++token; token != tokenizer.end(); ++token)
        {
            char c;
            int depth;
            unsigned long long nodes;

            std::istringstream ss(*token);
            ss >> c >> depth >> nodes;

            CAPTURE(fen);
            CAPTURE(depth);
            CAPTURE(nodes);

            p.set(fen);
            REQUIRE(perft(p, mg, depth) == nodes);
        }
    }
}
