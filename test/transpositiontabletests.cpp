#include "catch.hpp"

#include "transpositiontable.h"

using namespace idun;

TEST_CASE("Invalid transposition table entry.", "[transpositiontable]")
{
    TranspositionTable::Entry e;
    REQUIRE_FALSE(e.isValid());
}

TEST_CASE("Transposition table entry data.", "[transpositiontable]")
{
    constexpr int ply = 10;
    TranspositionTable::Entry e(0xdeadbeef,
                                TranspositionTable::NodeType::LowerBound, 1337,
                                42, ply, Move(Square(1), Square(2)));
    REQUIRE(e.isValid());

    REQUIRE(e.key() == 0xdeadbeef);
    REQUIRE(e.type() == TranspositionTable::NodeType::LowerBound);
    REQUIRE(e.value(ply) == 1337);
    REQUIRE(e.depth() == 42);
    REQUIRE(e.move() == Move(Square(1), Square(2)));
}

TEST_CASE("Table entry reads as invalid if read fails.", "[transpositiontable]")
{
    constexpr int ply = 10;
    TranspositionTable::Entry e(0xdeadbeef,
                                TranspositionTable::NodeType::LowerBound, 1337,
                                42, ply, Move(Square(1), Square(2)));

    SECTION("Key bit error")
    {
        uint64_t *data = reinterpret_cast<uint64_t *>(&e);
        data[0] ^= 0x0000000100000000;
    }

    SECTION("Data bit error")
    {
        uint64_t *data = reinterpret_cast<uint64_t *>(&e);
        data[1] ^= 0x0000000100000000;
    }

    REQUIRE(e.key() != 0xdeadbeef);
}
