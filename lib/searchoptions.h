#ifndef SEARCHOPTIONS_H
#define SEARCHOPTIONS_H

#include <string>
#include <vector>

namespace idun
{

class SearchOptions
{
public:
    SearchOptions();

    void clear();

    bool ponderSearch() const;
    void setPonderSearch(bool ponderSearch);

    bool mateSearch() const;
    void setMateSearch(bool mateSearch);

    bool infiniteSearch() const;
    void setInfiniteSearch(bool infiniteSearch);

    int depthLimit() const;
    void setDepthLimit(int depthLimit);

    int nodeLimit() const;
    void setNodeLimit(int nodeLimit);

    int timeLimit() const;
    void setTimeLimit(int timeLimit);

    const std::vector<std::string> &searchMoves() const;
    void setSearchMoves(const std::vector<std::string> &searchMoves);

    int whiteTimeLeft() const;
    void setWhiteTimeLeft(int whiteTimeLeft);

    int blackTimeLeft() const;
    void setBlackTimeLeft(int blackTimeLeft);

    int whiteIncrement() const;
    void setWhiteIncrement(int whiteIncrement);

    int blackIncrement() const;
    void setBlackIncrement(int blackIncrement);

    int movesToGo() const;
    void setMovesToGo(int movesToGo);

private:
    bool ponderSearch_;
    bool mateSearch_;
    bool infiniteSearch_;

    int depthLimit_;
    int nodeLimit_;
    int timeLimit_;

    std::vector<std::string> searchMoves_;

    // Time control
    int whiteTimeLeft_;
    int blackTimeLeft_;
    int whiteIncrement_;
    int blackIncrement_;
    int movesToGo_;
};
}

#endif // SEARCHOPTIONS_H
