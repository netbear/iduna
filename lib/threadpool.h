#ifndef THREADPOOL_H
#define THREADPOOL_H

#include <boost/asio/deadline_timer.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/thread.hpp>
#include <boost/thread/future.hpp>

#include <memory>
#include <type_traits>
#include <vector>

namespace idun
{

class ThreadPool
{
public:
    ThreadPool();

    void start();
    void start(unsigned threads);
    void stop();

    int threadCount() const;

    template <class Function>
    std::enable_if_t<
        std::is_void<typename std::result_of<Function()>::type>::value,
        boost::future<typename std::result_of<Function()>::type>>
    enqueue(Function &&function)
    {
        using Promise =
            boost::promise<typename std::result_of<Function()>::type>;
        auto promise = std::make_shared<Promise>();

        service_.post([promise, function]() {
            function();
            promise->set_value();
        });

        return promise->get_future();
    }

    template <class Function>
    std::enable_if_t<
        !std::is_void<typename std::result_of<Function()>::type>::value,
        boost::future<typename std::result_of<Function()>::type>>
    enqueue(Function &&function)
    {
        using Promise =
            boost::promise<typename std::result_of<Function()>::type>;
        auto promise = std::make_shared<Promise>();

        service_.post([promise, function]() {
            auto result = function();
            promise->set_value(result);
        });

        return promise->get_future();
    }

    template <class Function> void setTimer(long ms, Function &&callback)
    {
        auto timer = std::make_shared<boost::asio::deadline_timer>(
            service_, boost::posix_time::milliseconds(ms));
        timer->async_wait(
            [callback, timer](const boost::system::error_code &e) {
                if(e != boost::asio::error::operation_aborted)
                {
                    callback();
                }
            });
    }

private:
    boost::asio::io_service service_;
    boost::asio::io_service::work work_;
    std::vector<boost::thread> threads_;
};
}

#endif // THREADPOOL_H
