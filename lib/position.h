#ifndef POSITION_H
#define POSITION_H

#include "move.h"
#include "moveinformation.h"
#include "piece.h"
#include "repetitioncounter.h"
#include "types.h"

#include <array>
#include <limits>
#include <string>

namespace idun
{

class Position
{
public:
    Position();
    Position(const std::string &fen);

    /**
     * @brief Perform a move.
     *
     * @note The move is assumed to be valid. No validation is performed.
     *
     * @param move The move to perform.
     * @param moveInformation Pointer to a move information object used to undo
     * the move.
     */
    void doMove(Move move, MoveInformation *moveInformation);
    void undoMove(const MoveInformation *moveInfo);

    void doNullMove(MoveInformation *moveInformation);
    void undoNullMove(const MoveInformation *moveInfo);

    void clear();
    void set(const std::string &fen);
    std::string fen() const;

    Color activeColor() const;
    Color inactiveColor() const;
    bool whiteToMove() const;
    bool blackToMove() const;

    CastlingRights getCastlingRights() const;
    Square getEnPassantSquare() const;
    unsigned getHalfMoveClock() const;
    unsigned getFullMoveNumber() const;

    friend std::ostream &operator<<(std::ostream &os, const Position &position);

    bool operator==(const Position &position) const;
    bool operator!=(const Position &position) const;

    Piece get(Bitboard mask) const;
    Piece get(Square square) const;

    void put(Piece piece, Bitboard mask);
    void put(Piece piece, Square square);
    void remove(Piece piece, Bitboard mask);
    void remove(Piece piece, Square square);

    bool isOccupied(Square square) const;
    bool isOccupied(Bitboard bitboard) const;

    bool isEmpty(Square square) const;
    bool isEmpty(Bitboard bitboard) const;

    Bitboard getEmptyBitboard() const;
    Bitboard getOccupiedBitboard() const;

    Bitboard getFriendlyBitboard() const;
    Bitboard getHostileBitboard() const;

    Bitboard getBitboard(PieceType type) const;
    Bitboard getBitboard(Color color) const;
    Bitboard getBitboard(PieceType type, Color color) const;

    uint64_t hash() const;

    uint64_t calculateHash() const;

    unsigned repetitions() const;
    unsigned nodesVisited() const;

private:
    std::array<Bitboard, 6> typeBoard_;
    std::array<Bitboard, 2> colorBoard_;

    Color activeColor_;
    CastlingRights castlingRights_;
    Square enPassantSquare_;

    // This is the number of halfmoves since the last capture or pawn advance.
    // (fifty-move rule)
    unsigned halfMoveClock_;

    // Number of full moves starts at one. (Incremented after black's move)
    unsigned fullMoveNumber_;

    uint64_t hash_;

    RepetitionCounter repetitionCounter_;
    unsigned nodesVisited_ = 0;
};

std::ostream &operator<<(std::ostream &os, const Position &position);
}

#endif // POSITION_H
