#include "mask.h"

namespace idun
{

void Mask::init()
{
    if(!initialized_)
    {
        for(unsigned i = 0; i < 64; ++i)
        {
            for(unsigned j = 0; j < 64; ++j)
            {
                betweenExclusive_[i][j] = betweenGenerator_(i, j, false);
                betweenInclusive_[i][j] = betweenGenerator_(i, j, true);
            }
        }

        generate_(knightMoves_, Mask::knightMovesGenerator_);
        generate_(kingMoves_, Mask::kingMovesGenerator_);
        generate_(pawnAttacks_[static_cast<unsigned>(Color::WHITE)],
                  Mask::whitePawnAttacksGenerator_);
        generate_(pawnAttacks_[static_cast<unsigned>(Color::BLACK)],
                  Mask::blackPawnAttacksGenerator_);

        initialized_ = true;
    }
}

bool Mask::initialized_ = false;

constexpr Bitboard Mask::empty;
constexpr Bitboard Mask::center;

constexpr std::array<Bitboard, 8> Mask::rank_;
constexpr std::array<Bitboard, 8> Mask::file_;
constexpr std::array<Bitboard, 8> Mask::rightOf_;
constexpr std::array<Bitboard, 8> Mask::leftOf_;
constexpr std::array<Bitboard, 8> Mask::neighboringFiles_;
constexpr std::array<Bitboard, 15> Mask::diagonal_;
constexpr std::array<Bitboard, 15> Mask::antiDiagonal_;
constexpr std::array<Bitboard, 4> Mask::castlingMask_;
constexpr std::array<Bitboard, 4> Mask::castlingMaskIncKing_;

std::array<std::array<Bitboard, 64>, 64> Mask::betweenExclusive_;
std::array<std::array<Bitboard, 64>, 64> Mask::betweenInclusive_;

constexpr Bitboard Mask::notHFile_;
constexpr Bitboard Mask::notGHFile_;
constexpr Bitboard Mask::notAFile_;
constexpr Bitboard Mask::notABFile_;

std::array<Bitboard, 64> Mask::knightMoves_;
std::array<Bitboard, 64> Mask::kingMoves_;
std::array<std::array<Bitboard, 64>, 2> Mask::pawnAttacks_;

} // namespace idun
