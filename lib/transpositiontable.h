#ifndef IDUN_TRANSPOSITIONTABLE_H
#define IDUN_TRANSPOSITIONTABLE_H

#include <mutex>
#include <vector>

#include "move.h"
#include "types.h"

namespace idun
{

class TranspositionTable
{
public:
    using Key = uint64_t;

    enum class NodeType : uint8_t
    {
        Exact,
        UpperBound,
        LowerBound
    };

    struct Entry
    {
        Entry()
        {
            // Set depth to -1 to inidcate an invalid entry.
            data_ = (static_cast<uint64_t>(-1) << 32);
        }

        Entry(Key key, NodeType type, Score value, int16_t depth, int16_t ply,
              Move move)
        {
            if(value >= MATE_MAX_PLYS)
            {
                value += ply;
            }
            else if(value <= -MATE_MAX_PLYS)
            {
                value -= ply;
            }

            data_ = ((static_cast<uint64_t>(type) & 0x03) << 48) |
                    ((static_cast<uint64_t>(depth) & 0xffff) << 32) |
                    ((static_cast<uint64_t>(value) & 0xffff) << 16) |
                    static_cast<uint64_t>(*reinterpret_cast<int16_t *>(&move) &
                                          0xffff);

            key_ = key ^ data_;
        }

        Key key() const
        {
            return key_ ^ data_;
        }

        uint64_t data() const
        {
            return data_;
        }

        NodeType type() const
        {
            return static_cast<NodeType>((data_ >> 48) & 0x03);
        }

        int16_t depth() const
        {
            return static_cast<int16_t>((data_ >> 32) & 0xffff);
        }

        Score value(int16_t ply) const
        {
            Score v = static_cast<Score>((data_ >> 16) & 0xffff);
            if(v >= MATE_MAX_PLYS)
            {
                v -= ply;
            }
            else if(v <= -MATE_MAX_PLYS)
            {
                v += ply;
            }
            return v;
        }

        Move move() const
        {
            uint16_t move = static_cast<uint16_t>(data_ & 0xffff);
            return *reinterpret_cast<Move *>(&move);
        }

        bool isValid() const
        {
            return depth() >= 0;
        }

    private:
        Key key_;

        /*
         * Bits
         * 0-15    Move (16 bits)
         * 16-31   Value (16 bits)
         * 32-47   Depth (16 bits)
         * 48-49   Type
         */
        uint64_t data_;
    };

    TranspositionTable();

    const Entry lookup(Key key) const;
    void store(Entry entry);
    void clear();

    void resize(unsigned mib);

    uint64_t size() const;

private:
    uint64_t size_;
    uint64_t indexMask_;
    std::vector<Entry> table_;

    std::mutex mutex_;
};

} // namespace idun

#endif // IDUN_TRANSPOSITIONTABLE_H
