#include "timemanager.h"

#include <iostream>
#include <limits>

namespace idun
{

TimeManager::TimeManager() = default;

void TimeManager::init(const SearchOptions &searchOptions, Color activeColor,
                       unsigned moveNumber)
{
    activeColor_ = activeColor;
    depth_ = 128; // Max depth
    time_ = std::numeric_limits<long>::max();
    nodes_ = std::numeric_limits<long>::max();
    increment_ = 0;

    if(searchOptions.depthLimit() > 0)
    {
        type_ = TimeControl::CONST_DEPTH;
        depth_ = searchOptions.depthLimit();
    }
    else if(searchOptions.timeLimit() > 0)
    {
        type_ = TimeControl::CONST_TIME;
        time_ = searchOptions.timeLimit();
    }
    else if(searchOptions.nodeLimit() > 0)
    {
        type_ = TimeControl::CONST_NODES;
        nodes_ = searchOptions.nodeLimit();
    }
    else if(searchOptions.infiniteSearch())
    {
        type_ = TimeControl::INFINITE;
    }
    else
    {
        type_ = TimeControl::TIME;
        long timeLeft = (activeColor_ == Color::WHITE)
                            ? searchOptions.whiteTimeLeft()
                            : searchOptions.blackTimeLeft();

        increment_ = (activeColor_ == Color::WHITE)
                         ? searchOptions.whiteIncrement()
                         : searchOptions.blackIncrement();

        // If no next timecontrol assume a 20 moves (40 plys) sudden death game.
        int movesToGo = (searchOptions.movesToGo() > 0)
                            ? searchOptions.movesToGo()
                            : 20 - (static_cast<int>(moveNumber) - 1);

        // Compensate for increment.
        timeLeft += increment_ * movesToGo;

        // Assume the game will be 20 moves (40 plys) long.
        if(movesToGo > 0)
        {
            time_ = static_cast<long>((timeLeft * 0.9) / movesToGo);
        }
        else
        {
            // Play on increment.
            time_ = static_cast<long>(timeLeft * 0.05 + increment_ * 0.95);
        }
    }
}

bool TimeManager::update(int16_t /*depth*/, long time)
{
    // TODO: Find a better estimate.
    long estimatedTime = time * 4.0;
    return (time_ - time) > estimatedTime;
}

long TimeManager::depth() const
{
    return depth_;
}

bool TimeManager::isTimeLimited() const
{
    switch(type_)
    {
        case TimeControl::CONST_TIME:
        case TimeControl::TIME:
            return true;
        case TimeControl::CONST_DEPTH:
        case TimeControl::CONST_NODES:
        case TimeControl::INFINITE:
            return false;
    }

    return false;
}

long TimeManager::time() const
{
    return time_;
}

long TimeManager::nodes() const
{
    return nodes_;
}

} // namespace idun
