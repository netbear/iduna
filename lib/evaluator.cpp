#include "evaluator.h"

#include "bitboards.h"
#include "magic.h"
#include "mask.h"

namespace idun
{

Evaluator::Evaluator()
{
    Mask::init();
    Magic::init();
}

Score Evaluator::evaluate(const Position &position) const
{
    Score score = 0.0;
    bool endgame = getGameState(position) == GameState::End;

    // Material
    for(unsigned i = 0; i < 6; ++i)
    {
        const auto piece = static_cast<PieceType>(i);
        score += calculate(position.getBitboard(piece, Color::WHITE),
                           endgame ? pieceSquareTableEndgame[i]
                                   : pieceSquareTable[i],
                           pieceValue_[i], Color::WHITE);
        score -= calculate(position.getBitboard(piece, Color::BLACK),
                           endgame ? pieceSquareTableEndgame[i]
                                   : pieceSquareTable[i],
                           pieceValue_[i], Color::BLACK);
    }

    const Score pawnValue = pieceValue_[static_cast<unsigned>(PieceType::PAWN)];
    const Bitboard whitePawns =
        position.getBitboard(PieceType::PAWN, Color::WHITE);
    const Bitboard blackPawns =
        position.getBitboard(PieceType::PAWN, Color::BLACK);

    // Doubled pawns
    score -= 0.5 * pawnValue * (bb::popCount((whitePawns >> 8) & whitePawns) -
                                bb::popCount((blackPawns << 8) & blackPawns));

    // Blocked pawns
    score -= 0.5 * pawnValue * (bb::popCount((whitePawns >> 8) & blackPawns) -
                                bb::popCount((blackPawns << 8) & whitePawns));

    // Isolated pawns
    auto calculateIsolatedPawns = [](Bitboard pawns) {
        int n = 0;
        while(pawns)
        {
            int square = bb::bitScanAndClear(pawns);
            Bitboard mask = bb::getBitboard(square);
            if(mask & Mask::neighboringFiles(bb::getFile(square)))
            {
                ++n;
            }
        }
        return n;
    };

    int isolatedPawns =
        calculateIsolatedPawns(whitePawns) - calculateIsolatedPawns(blackPawns);
    score -= 0.5 * pawnValue * isolatedPawns;

    // Bounus for bishop pair
    const double bisopPairBonus = 0.25 * pawnValue;
    score +=
        bb::popCount(position.getBitboard(PieceType::BISHOP, Color::WHITE)) > 1
            ? bisopPairBonus
            : 0;
    score -=
        bb::popCount(position.getBitboard(PieceType::BISHOP, Color::BLACK)) > 1
            ? bisopPairBonus
            : 0;

    // Mobility
    //    int mobility = 0;

    //    const Bitboard white = position.getBitboard(Color::WHITE);
    //    const Bitboard black = position.getBitboard(Color::BLACK);
    //    const Bitboard empty = position.getEmptyBitboard();
    //    const Bitboard whiteAndEmpty = white | empty;
    //    const Bitboard blackAndEmpty = black | empty;
    //    const Bitboard occupied = position.getOccupiedBitboard();

    //    Bitboard mask(0x0000000000000001);
    //    for(int square = 0; square < 64; ++square, mask <<= 1)
    //    {
    //        auto piece = position.get(mask);
    //        const bool isWhite = piece.color() == Color::WHITE;
    //        const int factor = (isWhite?1:-1);

    //        const Bitboard moveMask = isWhite?blackAndEmpty:whiteAndEmpty;
    //        switch(piece.type()) {
    //            case PieceType::PAWN:
    //            {
    ////                const int adv = isWhite?8:-8;
    ////                const Bitboard possibleCaptures = Bitboard(0x05) <<
    ///(square-1+adv);
    ////                const Bitboard possibleMoves = (Bitboard(0x01) <<
    ///(square+adv)) | (Bitboard(0x01) << (square+2*adv));
    ////                const Bitboard moves = (possibleMoves & empty) |
    ///(possibleCaptures & (isWhite?black:white));
    ////                mobility += factor * bb::popCount(moves);
    //                break;
    //            }
    //            case PieceType::KNIGHT:
    //            {
    //                const Bitboard possibleMoves =
    //                bb::knightMoves[static_cast<unsigned>(square)];
    //                const Bitboard moves = possibleMoves & moveMask;
    //                mobility += 1 * factor * bb::popCount(moves);
    //                break;
    //            }
    //            case PieceType::BISHOP:
    //            {
    //                const Bitboard possibleMoves =
    //                Magic::getBishopMoves(square, occupied);
    //                const Bitboard moves = possibleMoves & moveMask;
    //                mobility += 1 * factor * bb::popCount(moves);
    //                break;
    //            }
    //            case PieceType::ROOK:
    //            {
    //                const Bitboard possibleMoves = Magic::getRookMoves(square,
    //                occupied);
    //                const Bitboard moves = possibleMoves & moveMask;
    //                mobility += 1 * factor * bb::popCount(moves);
    //                break;
    //            }
    //            case PieceType::QUEEN:
    //            {
    //                const Bitboard possibleMoves =
    //                Magic::getQueenMoves(square, occupied);
    //                const Bitboard moves = possibleMoves & moveMask;
    //                mobility += 1 * factor * bb::popCount(moves);
    //                break;
    //            }
    //            case PieceType::KING:
    //            {
    ////                const Bitboard possibleMoves =
    ///bb::kingMoves[static_cast<unsigned>(square)];
    ////                const Bitboard moves = possibleMoves & moveMask;
    ////                mobility += factor * bb::popCount(moves);
    //                break;
    //            }
    //            case PieceType::NONE:
    //            {
    //                break;
    //            }
    //        }
    //    }

    //    score += mobility;

    return score;
}

Score Evaluator::pieceValue(PieceType pieceType) const
{
    return pieceValue_[static_cast<unsigned>(pieceType)];
}

Evaluator::GameState Evaluator::getGameState(const Position &position) const
{
    const Bitboard queens = position.getBitboard(PieceType::QUEEN);
    const Bitboard whiteQueens =
        position.getBitboard(PieceType::QUEEN, Color::WHITE);
    const Bitboard blackQueens =
        position.getBitboard(PieceType::QUEEN, Color::BLACK);
    const Bitboard whitePieces =
        position.getBitboard(PieceType::KNIGHT, Color::WHITE) |
        position.getBitboard(PieceType::BISHOP, Color::WHITE) |
        position.getBitboard(PieceType::ROOK, Color::WHITE);
    const Bitboard blackPieces =
        position.getBitboard(PieceType::KNIGHT, Color::BLACK) |
        position.getBitboard(PieceType::BISHOP, Color::BLACK) |
        position.getBitboard(PieceType::ROOK, Color::BLACK);

    bool endgame = (queens == Mask::empty) ||
                   ((whiteQueens && bb::popCount(whitePieces) < 2) &&
                    (blackQueens && bb::popCount(blackPieces) < 2));

    return endgame ? GameState::End : GameState::Opening;
}

Score Evaluator::calculate(Bitboard pieces, Evaluator::SquareTable table,
                           Score pieceValue, Color color) const
{
    Score score = 0;
    while(pieces)
    {
        const int square = bb::bitScanAndClear(pieces);
        score += table[squareIndex(color, square)] + pieceValue;
    }
    return score;
}

constexpr std::array<Score, 7> Evaluator::pieceValue_;
constexpr std::array<std::array<Score, 64>, 6> Evaluator::pieceSquareTable;
constexpr std::array<std::array<Score, 64>, 6>
    Evaluator::pieceSquareTableEndgame;
}
