#ifndef ENUMBITFIELD_H
#define ENUMBITFIELD_H

#include <type_traits>

namespace idun
{

template <typename E> struct enable_enum_bitfield_operators
{
    static const bool enable = false;
};

template <typename E>
auto operator|(E lhs, E rhs) ->
    typename std::enable_if<enable_enum_bitfield_operators<E>::enable, E>::type
{
    typedef typename std::underlying_type<E>::type underlying;
    return static_cast<E>(static_cast<underlying>(lhs) |
                          static_cast<underlying>(rhs));
}

template <typename E>
auto operator&(E lhs, E rhs) ->
    typename std::enable_if<enable_enum_bitfield_operators<E>::enable, E>::type
{
    typedef typename std::underlying_type<E>::type underlying;
    return static_cast<E>(static_cast<underlying>(lhs) &
                          static_cast<underlying>(rhs));
}

template <typename E>
auto operator^(E lhs, E rhs) ->
    typename std::enable_if<enable_enum_bitfield_operators<E>::enable, E>::type
{
    typedef typename std::underlying_type<E>::type underlying;
    return static_cast<E>(static_cast<underlying>(lhs) ^
                          static_cast<underlying>(rhs));
}

template <typename E>
auto operator~(E lhs) ->
    typename std::enable_if<enable_enum_bitfield_operators<E>::enable, E>::type
{
    typedef typename std::underlying_type<E>::type underlying;
    return static_cast<E>(~static_cast<underlying>(lhs));
}

template <typename E>
auto operator|=(E &lhs, E rhs) ->
    typename std::enable_if<enable_enum_bitfield_operators<E>::enable,
                            E &>::type
{
    typedef typename std::underlying_type<E>::type underlying;
    lhs = static_cast<E>(static_cast<underlying>(lhs) |
                         static_cast<underlying>(rhs));
    return lhs;
}

template <typename E>
auto operator&=(E &lhs, E rhs) ->
    typename std::enable_if<enable_enum_bitfield_operators<E>::enable,
                            E &>::type
{
    typedef typename std::underlying_type<E>::type underlying;
    lhs = static_cast<E>(static_cast<underlying>(lhs) &
                         static_cast<underlying>(rhs));
    return lhs;
}

template <typename E>
auto operator^=(E &lhs, E rhs) ->
    typename std::enable_if<enable_enum_bitfield_operators<E>::enable,
                            E &>::type
{
    typedef typename std::underlying_type<E>::type underlying;
    lhs = static_cast<E>(static_cast<underlying>(lhs) ^
                         static_cast<underlying>(rhs));
    return lhs;
}
}

#endif // ENUMBITFIELD_H
