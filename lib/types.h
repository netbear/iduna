#ifndef TYPES_H
#define TYPES_H

#include <cassert>
#include <cinttypes>
#include <limits>

#include "integertype.h"

namespace idun
{

struct SquareTag
{
};
using Square = IntegerType<int, SquareTag>;
constexpr Square INVALID_SQUARE =
    Square(std::numeric_limits<Square::base_type>::max());

struct BitboardTag
{
};
using Bitboard = IntegerType<uint64_t, BitboardTag>;

using Score = int16_t;
using MateScore = int16_t;

constexpr int16_t MAX_PLYS = 128;
constexpr Score MATE_MAX_PLYS = std::numeric_limits<Score>::max() - MAX_PLYS;

constexpr Score mateIn(int16_t plys)
{
    return std::numeric_limits<Score>::max() - plys;
}

constexpr Score matedIn(int16_t plys)
{
    return -std::numeric_limits<Score>::max() + plys;
}

/**
 * @brief Return number of plies to mate.
 * @param score Positoion score.
 * @return Number of plies to mate including sign, if not a mate score 0 is
 * returned.
 */
constexpr Score mateDistance(Score score)
{
    if(score >= MATE_MAX_PLYS)
    {
        return std::numeric_limits<Score>::max() - score;
    }
    else if(score <= -MATE_MAX_PLYS)
    {
        return -std::numeric_limits<Score>::max() - score;
    }
    else
    {
        return 0;
    }
}

enum class PieceType
{
    PAWN = 0,
    KNIGHT = 1,
    BISHOP = 2,
    ROOK = 3,
    QUEEN = 4,
    KING = 5,
    NONE = 6
};

enum class Color
{
    WHITE = 0,
    BLACK = 1,
    NONE = 2
};

inline Color invert(Color color)
{
    return color == Color::WHITE ? Color::BLACK : Color::WHITE;
}
}

#endif // TYPES_H
