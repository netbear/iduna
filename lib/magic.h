#ifndef MAGIC_H
#define MAGIC_H

#include <array>
#include <cinttypes>
#include <vector>

#include "types.h"

namespace idun
{

class Magic
{
public:
    /**
     * @brief Initialize lookup tables.
     * This method needs to be called once before the magic bitboards are used.
     */
    static void init();

    static void findMagicNumbers(bool bishop);
    static uint64_t findMagicNumber(int square, unsigned bits, bool bishop);

    /**
     * @brief Get possible rook moves.
     *
     * @note The generated moves include self captures.
     *
     * @param square Square for the rook to generate moves for.
     * @param occupancyMask Mask with all occupied squares.
     * @return Mask with all available destination squares including slef
     * captures.
     */
    static Bitboard getRookMoves(int square, Bitboard occupancyMask);

    /**
     * @brief Get possible bishop moves.
     *
     * @note The generated moves include self captures.
     *
     * @param square Square for the bishop to generate moves for.
     * @param occupancyMask Mask with all occupied squares.
     * @return Mask with all available destination squares including slef
     * captures.
     */
    static Bitboard getBishopMoves(int square, Bitboard occupancyMask);

    /**
     * @brief Get possible queen moves.
     *
     * @note The generated moves include self captures.
     *
     * @param square Square for the queen to generate moves for.
     * @param occupancyMask Mask with all occupied squares.
     * @return Mask with all available destination squares including slef
     * captures.
     */
    static Bitboard getQueenMoves(int square, Bitboard occupancyMask);

private:
    struct MagicNumber
    {
        uint64_t number;
        unsigned bits;
    };

    struct SMagic
    {
        Bitboard *table;
        Bitboard mask;
        uint64_t magic;
        unsigned shift;
    };

    static Bitboard getMoves(const SMagic &smagic, Bitboard occupancyMask);

    constexpr static Bitboard occupancyMaskRook(int square);
    constexpr static Bitboard occupancyMaskBishop(int square);

    constexpr static Bitboard
    occupancyVariations(Bitboard occupancyMask, unsigned bits, unsigned index);

    constexpr static Bitboard moveBoard(Bitboard occupancyVariation, int square,
                                        const std::array<int8_t, 4> &shifts);
    constexpr static Bitboard moveBoardRook(Bitboard occupancyVariation,
                                            int square);
    constexpr static Bitboard moveBoardBishop(Bitboard occupancyVariation,
                                              int square);

    constexpr static Bitboard attackSet(Bitboard occupancyVariation, int square,
                                        const std::array<int8_t, 4> &shifts);
    constexpr static Bitboard attackSetRook(Bitboard occupancyVariation,
                                            int square);
    constexpr static Bitboard attackSetBishop(Bitboard occupancyVariation,
                                              int square);

    static bool initialized_;

    static std::vector<Bitboard> attackTable_;
    static std::array<SMagic, 64> bishopTable_;
    static std::array<SMagic, 64> rookTable_;

    static std::array<MagicNumber, 64> rookMagicNumbers_;
    static std::array<MagicNumber, 64> bishopMagicNumbers_;
};
}

#endif // MAGIC_H
