#ifndef MATH_H
#define MATH_H

#include <limits>

namespace idun
{

template <typename Value> constexpr Value sign(Value value)
{
    return (Value(0) < value) - (value < Value(0));
}

template <typename Value> constexpr Value safeSymetricAddition(Value a, Value b)
{
    if((b > 0) && a > std::numeric_limits<Value>::max() - b)
    {
        return std::numeric_limits<Value>::max();
    }
    else if((b < 0) && a < -std::numeric_limits<Value>::max() - b)
    {
        return -std::numeric_limits<Value>::max();
    }
    else
    {
        return a + b;
    }
}

template <typename Value>
constexpr Value safeSymetricSubstraction(Value a, Value b)
{
    if((b < 0) && a > std::numeric_limits<Value>::max() + b)
    {
        return std::numeric_limits<Value>::max();
    }
    else if((b > 0) && a < -std::numeric_limits<Value>::max() + b)
    {
        return -std::numeric_limits<Value>::max();
    }
    else
    {
        return a - b;
    }
}
}

#endif // MATH_H
