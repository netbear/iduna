#ifndef IDUN_ZOBRIST_H
#define IDUN_ZOBRIST_H

#include <array>

#include "bitboards.h"
#include "position.h"

#include <cassert>

namespace idun
{

class Zobrist
{
public:
    static void init();

    inline static uint64_t pieceValue(Color color, PieceType pieceType,
                                      Square square)
    {
        assert(initialized_);
        return piece_[static_cast<unsigned>(color)][static_cast<unsigned>(
            pieceType)][static_cast<unsigned>(square)];
    }

    inline static uint64_t blackToMove()
    {
        assert(initialized_);
        return blackToMove_;
    }

    inline static uint64_t castling(unsigned index)
    {
        assert(initialized_);
        return castling_[index];
    }

    inline static uint64_t enPassantFile(Square square)
    {
        assert(initialized_);
        return enPassantFile_[static_cast<unsigned>(square) % 8];
    }

private:
    static bool initialized_;

    static std::array<std::array<std::array<uint64_t, 64>, 6>, 2> piece_;
    static uint64_t blackToMove_;
    static std::array<uint64_t, 16> castling_;
    static std::array<uint64_t, 8> enPassantFile_;
};

} // namespace idun

#endif // IDUN_ZOBRIST_H
