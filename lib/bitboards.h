#ifndef BITBOARDS_H
#define BITBOARDS_H

#include "types.h"

#include <iostream>

namespace idun
{
namespace bb
{
template <class T> constexpr Bitboard getBitboard(T square)
{
    return Bitboard(static_cast<uint64_t>(1) << static_cast<int>(square));
}

template <class T> constexpr int popCount(const T &bitboard)
{
    return __builtin_popcountll(static_cast<unsigned long long>(bitboard));
}

template <class T> constexpr Square::base_type bitScan(const T &bitboard)
{
    assert(bitboard);
    return __builtin_ctzll(static_cast<unsigned long long>(bitboard));
}

template <class T> constexpr int bitScanAndClear(T &bitboard)
{
    assert(bitboard);
    int square = bitScan(bitboard);
    Bitboard mask = getBitboard(square);
    bitboard &= ~mask;
    return square;
}

template <class T> constexpr Square getSquare(T bitboard)
{
    return Square(bb::bitScan(bitboard));
}

constexpr Square getSquare(int file, int rank)
{
    return Square(file + 8 * rank);
}

template <class T> constexpr std::pair<int, int> getFileAndRank(T square)
{
    const int s = static_cast<int>(square);
    return std::make_pair<int, int>(s % 8, s / 8);
}

template <class T> constexpr int getFile(T square)
{
    const int s = static_cast<int>(square);
    return s % 8;
}

template <class T> constexpr int getRank(T square)
{
    const int s = static_cast<int>(square);
    return s / 8;
}

constexpr Bitboard getBitboard(int file, int rank)
{
    return getBitboard(getSquare(file, rank));
}

constexpr int getDiagonal(int file, int rank)
{
    return file + rank;
}

constexpr int getAntiDiagonal(int file, int rank)
{
    return 7 - file + rank;
}

template <class T> void print(T bb, std::string title = "")
{
    if(!title.empty())
    {
        std::cout << title << '\n';
    }
    for(int y = 0; y < 8; ++y)
    {
        std::cout << "+---+---+---+---+---+---+---+---+\n| ";
        for(int x = 0; x < 8; ++x)
        {

            if(bb & (Bitboard(0x8000000000000000)))
            {
                std::cout << "X"
                          << " | ";
            }
            else
            {
                std::cout << "  | ";
            }

            bb <<= 1;
        }
        std::cout << (7 - y) * 8 << "\n";
    }
    std::cout << "+---+---+---+---+---+---+---+---+\n";
}
}
}

#endif // BITBOARDS_H
