#include "zobrist.h"

#include <random>

namespace idun
{

void Zobrist::init()
{
    if(!initialized_)
    {
        std::mt19937_64 generator(0xdeadbeef);

        for(unsigned c = 0; c < 2; ++c)
        {
            for(unsigned p = 0; p < 6; ++p)
            {
                for(unsigned s = 0; s < 64; ++s)
                {
                    piece_[c][p][s] = generator();
                }
            }
        }

        blackToMove_ = generator();

        for(auto &file : enPassantFile_) {
            file = generator();
        }

        castling_[0] = 0x0000000000000000;
        castling_[1] = generator();
        castling_[2] = generator();
        castling_[3] = castling_[1] ^ castling_[2];
        castling_[4] = generator();
        castling_[5] = castling_[1] ^ castling_[4];
        castling_[6] = castling_[2] ^ castling_[4];
        castling_[7] = castling_[1] ^ castling_[2] ^ castling_[4];
        castling_[8] = generator();
        castling_[9] = castling_[1] ^ castling_[8];
        castling_[10] = castling_[2] ^ castling_[8];
        castling_[11] = castling_[1] ^ castling_[2] ^ castling_[8];
        castling_[12] = castling_[4] ^ castling_[8];
        castling_[13] = castling_[1] ^ castling_[4] ^ castling_[8];
        castling_[14] = castling_[2] ^ castling_[4] ^ castling_[8];
        castling_[15] =
            castling_[1] ^ castling_[2] ^ castling_[4] ^ castling_[8];

        initialized_ = true;
    }
}

bool Zobrist::initialized_;

std::array<std::array<std::array<uint64_t, 64>, 6>, 2> Zobrist::piece_;
uint64_t Zobrist::blackToMove_;
std::array<uint64_t, 16> Zobrist::castling_;
std::array<uint64_t, 8> Zobrist::enPassantFile_;

} // namespace idun
