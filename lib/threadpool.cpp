#include "threadpool.h"

namespace idun
{

ThreadPool::ThreadPool() : work_(service_)
{
}

void ThreadPool::start()
{
    unsigned n = boost::thread::hardware_concurrency();
    start(n);
}

void ThreadPool::start(unsigned threads)
{
    threads_.resize(threads);
    for(unsigned i = 0; i < threads; ++i)
    {
        threads_[i] = boost::thread([this]() { this->service_.run(); });
    }
}

void ThreadPool::stop()
{
    service_.stop();

    for(auto &thread : threads_)
    {
        thread.join();
    }
}

int ThreadPool::threadCount() const
{
    return threads_.size();
}
}
