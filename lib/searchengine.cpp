#include "searchengine.h"

#include <algorithm>
#include <chrono>
#include <cstring>
#include <limits>

#include "bitboards.h"
#include "threadpool.h"
#include "timemanager.h"

#include "movepicker.h"

#include "mathutils.h"

namespace idun
{

SearchEngine::SearchEngine()
{
    threadPool_.start();
}

SearchEngine::~SearchEngine()
{
    threadPool_.stop();
}

boost::signals2::connection
SearchEngine::subscribeInfo(const InfoSignal::slot_type &subscriber)
{
    return infoSignal_.connect(subscriber);
}

void SearchEngine::newGame()
{
}

Move SearchEngine::run(Position position, const SearchOptions &options,
                       std::atomic<bool> &running)
{
    auto startTime = std::chrono::high_resolution_clock::now();

    timeManager_.init(options, position.activeColor(),
                      position.getFullMoveNumber());

    if(timeManager_.isTimeLimited())
    {
        threadPool_.setTimer(timeManager_.time(),
                             [&running]() { running = false; });
    }

    Move bestMove;

    Score alpha = -std::numeric_limits<idun::Score>::max();
    Score beta = std::numeric_limits<idun::Score>::max();

    int color = position.activeColor() == idun::Color::WHITE ? 1 : -1;

    ThreadData thread;
    std::array<PlyData, MAX_PLYS + 4> plyStack;
    auto rootPly = plyStack.data() + 2;

    for(int16_t currentDepth = 1; currentDepth <= timeManager_.depth();
        ++currentDepth)
    {
        Score score = 0;
        Score mateInPlies = 0;

        // Aspiration window loop
        while(running)
        {
            score = search(NodeType::Root, thread, rootPly, position,
                           currentDepth, alpha, beta, color, true, running);

            if(position.nodesVisited() >= timeManager_.nodes())
            {
                running = false;
            }

            mateInPlies = mateDistance(score);

            if(mateInPlies == 0) {
                if(score < alpha)
                { // Fail low
                    alpha = safeSymetricSubstraction(score, Score(100));
                }
                else if(score >= beta)
                { // Fail high
                    beta = safeSymetricAddition(score, Score(100));
                }
            }
            else {
                break;
            }
        };

        // Update alpha and beta for the next depth
        alpha = safeSymetricSubstraction(score, Score(100 / 4));
        beta = safeSymetricAddition(score, Score(100 / 4));

        if(running)
        {
            auto now = std::chrono::high_resolution_clock::now();
            auto duration = now - startTime;
            auto ns =
                std::chrono::duration_cast<std::chrono::nanoseconds>(duration)
                    .count();
            auto ms =
                std::chrono::duration_cast<std::chrono::milliseconds>(duration)
                    .count();

            // Give the time manager something to work with
            running = timeManager_.update(currentDepth, ms);

            // Send current principal variation
            PrincipalVariation pv;
            pv.time = ms;
            pv.moves = extractPrincipalVariation(position, score);

            if(mateInPlies != 0)
            {
                pv.scoreType = PrincipalVariation::ScoreType::Mate;
                pv.score = (mateInPlies > 0) ? std::floor(mateInPlies / 2.0)
                                             : std::ceil(mateInPlies / 2.0);
            }
            else
            {
                pv.scoreType = PrincipalVariation::ScoreType::CentiPawns;
                pv.score = score;
            }

            pv.depth = currentDepth;
            pv.selectiveDepth = thread.maxDepth;
            pv.nodes = position.nodesVisited();
            pv.nodesPerSecond = static_cast<unsigned>(
                (position.nodesVisited() / (double)ns) * 10e9);

            assert(!pv.moves.empty());

            infoSignal_(pv);

            // Record the currently best move
            bestMove = pv.moves.front();
        }

        if(!running)
        {
            break;
        }
    }
    return bestMove;
}

void SearchEngine::setTranspositionTableSize(unsigned mib)
{
    transpositionTable_.resize(mib);
}

void SearchEngine::clearTranspositionTable()
{
    transpositionTable_.clear();
}

Score SearchEngine::search(NodeType nodeType, ThreadData &thread, PlyData *ply,
                           Position &position, int16_t depth, Score alpha,
                           Score beta, int color, bool doPruning,
                           std::atomic<bool> &running)
{
    // Update ply number
    ply->ply = (ply - 1)->ply + 1;

    // Update max depth
    thread.maxDepth = std::max(thread.maxDepth, ply->ply);

    Score alphaOrig = alpha;
    const uint64_t hash = position.hash();

    if(position.getHalfMoveClock() >= 100 || position.repetitions() >= 3)
    {
        // Draw
        return Score(0);
    }

    // Lookup transposition table
    TranspositionTable::Entry tableEntry = transpositionTable_.lookup(hash);
    if(tableEntry.isValid() && tableEntry.depth() >= depth)
    {
        switch(tableEntry.type())
        {
            case TranspositionTable::NodeType::Exact:
                // It is faster (and gives longer pvs) if the exact value is not
                // used. This is probably due to a hotter transposition table
                // where the current pv is reinserted and the fact that pv nodes
                // are rare.
                break;
            case TranspositionTable::NodeType::LowerBound:
                alpha = std::max(alpha, tableEntry.value(ply->ply));
                break;
            case TranspositionTable::NodeType::UpperBound:
                beta = std::min(beta, tableEntry.value(ply->ply));
                break;
        }

        if(alpha >= beta)
        {
            return tableEntry.value(ply->ply);
        }
    }

    if(!running)
    {
        return static_cast<Score>(color) * evaluator_.evaluate(position);
    }

    if(depth <= 0)
    {
        return quiescence(NodeType::Pv, thread, ply - 1, position, alpha, beta,
                          color, running);
    }

    bool inCheck = moveGenerator_.isKingAttacked(position);
    if(doPruning && !inCheck && nodeType != NodeType::Root)
    {
        Score eval = static_cast<Score>(color) * evaluator_.evaluate(position);

        // Futility pruning
        const Score futilityMargin = depth * 100;
        if(depth < 7 && (eval - futilityMargin) >= beta)
        {
            return eval;
        }

        // Null-move pruning
        const int nullMoveDepthReduction = 3;
        if((depth >= nullMoveDepthReduction - 1) && (eval >= beta || depth < 4))
        {
            // Detect zugzwang
            Color toMove = color == 1 ? Color::WHITE : Color::BLACK;
            auto excluded = position.getBitboard(PieceType::PAWN, toMove) |
                            position.getBitboard(PieceType::KING, toMove);
            auto movingPieces = position.getBitboard(toMove) & ~(excluded);

            if(movingPieces)
            {
                MoveInformation mi;
                position.doNullMove(&mi);
                Score nullMoveScore =
                    -search(NodeType::Pv, thread, ply + 1, position,
                            depth - 1 - nullMoveDepthReduction, -beta,
                            -beta + 1, -color, false, running);
                position.undoNullMove(&mi);

                if(nullMoveScore >= beta)
                {
                    // We should probably not trust mate scores here, e.g.
                    // if(nullMoveScore >= MATE_MAX_PLYS) return beta;
                    // but not doing so actually makes the engine worse.
                    // TODO: Investigate

                    return nullMoveScore;
                }
            }
        }
    }

    // Get legal moves
    auto moves = moveGenerator_.generateLegalMoves(position);

    // Check terminal state
    if(moves.empty())
    {
        if(inCheck)
        {
            // Check mate
            // Negative factor since the color to move is checkmated.
            return matedIn(ply->ply);
        }

        // Stale mate
        return Score(0);
    }

    // Internal Iterative Deepening (IID)
    if((!tableEntry.isValid() || !tableEntry.move().isValid()) && depth > 2)
    {
        SearchEngine::search(nodeType, thread, ply, position,
                             depth > 4 ? depth / 2 : depth - 2, alpha, beta,
                             color, doPruning, running);

        // New hash lookup
        tableEntry = transpositionTable_.lookup(hash);
    }

    // Initialize move picker
    Move pvMove;
    if(tableEntry.isValid())
    {
        pvMove = tableEntry.move();
    }
    MovePicker movePicker(pvMove, position, moves, thread, ply);

    // Search
    Move bestMove;
    Score bestScore = -std::numeric_limits<Score>::max();

    while(movePicker.hasNext())
    {
        const Move &move = movePicker.next();

        MoveInformation mi;
        position.doMove(move, &mi);

        Score score =
            -search(NodeType::Pv, thread, ply + 1, position, depth - 1, -beta,
                    -alpha, -color, doPruning, running);

        position.undoMove(&mi);

        if(score > bestScore)
        {
            bestMove = move;
            bestScore = score;

            if(score >= beta)
            {
                if(!mi.isCapture())
                {
                    // Update move counter
                    thread.moveCounter.count(bestMove, mi.movedPiece(),
                                             static_cast<unsigned long>(1));

                    // Store killer move
                    if(ply->killerMoves[0] != move)
                    {
                        ply->killerMoves[1] = ply->killerMoves[0];
                        ply->killerMoves[0] = move;
                    }
                }

                break; // beta-cutoff
            }

            alpha = std::max(alpha, score);
        }
    }

    assert(position.hash() == hash);

    // Store result in transposition table
    if(running)
    {
        TranspositionTable::NodeType type;
        if(bestScore < alphaOrig)
        {
            type = TranspositionTable::NodeType::UpperBound;
        }
        else if(bestScore >= beta)
        {
            type = TranspositionTable::NodeType::LowerBound;
        }
        else
        {
            type = TranspositionTable::NodeType::Exact;
        }
        transpositionTable_.store(TranspositionTable::Entry(
            hash, type, bestScore, depth, ply->ply, bestMove));
    }

    return bestScore;
}

Score SearchEngine::quiescence(NodeType nodeType, ThreadData &thread,
                               PlyData *ply, Position &position, Score alpha,
                               Score beta, int color,
                               std::atomic<bool> &running)
{
    // Update ply number
    ply->ply = (ply - 1)->ply + 1;

    // Update max depth
    thread.maxDepth = std::max(thread.maxDepth, ply->ply);

    if(position.getHalfMoveClock() >= 100 || position.repetitions() >= 3)
    {
        return Score(0); // Draw
    }

    // Standard pat
    Score eval = static_cast<Score>(color) * evaluator_.evaluate(position);
    if(!running)
    {
        return eval;
    }

    if(eval >= beta)
    {
        return eval;
    }

    if(eval > alpha)
    {
        alpha = eval;
    }

    // Search
    std::vector<Move> moves = moveGenerator_.generateLegalMoves(
        position, MoveGenerator::MoveType::Capture);
    MovePicker movePicker(Move(), position, moves, thread, ply);

    while(movePicker.hasNext())
    {
        const Move &move = movePicker.next();

        MoveInformation mi;
        position.doMove(move, &mi);

        Score score = -quiescence(nodeType, thread, ply + 1, position, -beta,
                                  -alpha, -color, running);

        position.undoMove(&mi);

        alpha = std::max(alpha, score);
        if(score >= beta)
        {
            break;
        }
    }

    return alpha;
}

std::vector<Move> SearchEngine::extractPrincipalVariation(Position pos,
                                                          Score score)
{
    std::vector<Move> pv;

    int16_t ply = 1;

    MoveInformation mi;
    while(pos.getHalfMoveClock() < 100 || pos.repetitions() < 3)
    {
        TranspositionTable::Entry tableEntry =
            transpositionTable_.lookup(pos.hash());
        if(tableEntry.isValid() && tableEntry.move().isValid() &&
           tableEntry.value(ply++) == score)
        {
            assert(moveGenerator_.isLegal(pos, tableEntry.move()));

            pv.push_back(tableEntry.move());
            pos.doMove(tableEntry.move(), &mi);

            score = -score;
        }
        else
        {
            break;
        }
    }

    return pv;
}
}
