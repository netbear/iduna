#ifndef PIECE_H
#define PIECE_H

#include <cinttypes>

#include "types.h"

namespace idun
{

class Piece
{
public:
    explicit Piece(PieceType type = PieceType::NONE, Color color = Color::NONE);

    bool isValid() const;

    Color color() const;
    PieceType type() const;

    bool operator==(const Piece &piece) const;
    bool operator!=(const Piece &piece) const;

private:
    /**
     * @brief Encodes data for the piece.
     *
     *  0-2 PieceType
     *  3-4 Color
     *  5-7 Unused
     *
     */
    uint8_t data_;
};
}

#endif // PIECE_H
