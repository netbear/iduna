#include "piece.h"

namespace idun
{

Piece::Piece(PieceType type, Color color)
{
    data_ = (static_cast<uint8_t>(type) & 0x07) |
            ((static_cast<uint8_t>(color) & 0x03) << 3);
}

bool Piece::isValid() const
{
    return *this != Piece(PieceType::NONE, Color::NONE);
}

Color Piece::color() const
{
    return static_cast<Color>((data_ >> 3) & 0x03);
}

PieceType Piece::type() const
{
    return static_cast<PieceType>(data_ & 0x07);
}

bool Piece::operator==(const Piece &piece) const
{
    return data_ == piece.data_;
}

bool Piece::operator!=(const Piece &piece) const
{
    return !(*this == piece);
}
}
