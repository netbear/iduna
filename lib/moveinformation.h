#ifndef MOVEINFORMATION_H
#define MOVEINFORMATION_H

#include "castlingrights.h"
#include "move.h"
#include "piece.h"
#include "types.h"

namespace idun
{

class MoveInformation
{
public:
    MoveInformation();

    Move move() const;
    void setMove(const Move &move);

    Piece capturedPiece() const;
    void setCapturedPiece(const Piece &capturedPiece);

    Piece movedPiece() const;
    void setMovedPiece(const Piece &movedPiece);

    Color color() const;
    void setColor(const Color &color);

    Square previousEnPassantSquare() const;
    void setPreviousEnPassantSquare(const Square &previousEnPassantSquare);

    CastlingRights previousCastlingRights() const;
    void
    setPreviousCastlingRights(const CastlingRights &previousCastlingRights);

    unsigned previousHalfMoveClock() const;
    void setPreviousHalfMoveClock(const unsigned &previousHalfMoveClock);

    uint64_t hash() const;
    void setHash(const uint64_t &hash);

    bool isCapture() const;

private:
    // Todo: Compress.
    Move move_;
    Piece capturedPiece_;
    Piece movedPiece_;
    Color color_;
    Square previousEnPassantSquare_;
    CastlingRights previousCastlingRights_;
    unsigned previousHalfMoveClock_;
    uint64_t hash_;
};
}

#endif // MOVEINFORMATION_H
