#include "movecounter.h"

#include <algorithm>

namespace idun
{

MoveCounter::MoveCounter()
{
    reset();
}

void MoveCounter::reset()
{
    total_ = 0;
    for(auto &n : counter_) {
        std::fill(n.begin(), n.end(), 0);
    }
}

void MoveCounter::count(Move move, Piece movedPiece, unsigned long increment)
{
    ++total_;
    const unsigned pieceIndex = static_cast<unsigned>(movedPiece.type()) *
                                (static_cast<unsigned>(movedPiece.color()) + 1);
    counter_[pieceIndex][static_cast<unsigned>(move.destination())] +=
        increment;
}

unsigned long MoveCounter::getCount(Move move, Piece movedPiece) const
{
    const unsigned pieceIndex = static_cast<unsigned>(movedPiece.type()) *
                                (static_cast<unsigned>(movedPiece.color()) + 1);
    return counter_[pieceIndex][static_cast<unsigned>(move.destination())];
}

double MoveCounter::getNormalizedCount(Move move, Piece movedPiece) const
{
    auto count = getCount(move, movedPiece);
    return (total_ != 0) ? count / total_ : 0;
}

} // namespace idun
