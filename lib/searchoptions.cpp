#include "searchoptions.h"

namespace idun
{

SearchOptions::SearchOptions()
{
    clear();
}

void SearchOptions::clear()
{
    ponderSearch_ = false;
    mateSearch_ = false;
    infiniteSearch_ = false;

    depthLimit_ = -1;
    nodeLimit_ = -1;
    timeLimit_ = -1;

    searchMoves_.clear();

    whiteTimeLeft_ = -1;
    blackTimeLeft_ = -1;
    whiteIncrement_ = 0;
    blackIncrement_ = 0;
    movesToGo_ = -1;
}

bool SearchOptions::ponderSearch() const
{
    return ponderSearch_;
}

void SearchOptions::setPonderSearch(bool ponderSearch)
{
    ponderSearch_ = ponderSearch;
}

bool SearchOptions::mateSearch() const
{
    return mateSearch_;
}

void SearchOptions::setMateSearch(bool mateSearch)
{
    mateSearch_ = mateSearch;
}

bool SearchOptions::infiniteSearch() const
{
    return infiniteSearch_;
}

void SearchOptions::setInfiniteSearch(bool infiniteSearch)
{
    infiniteSearch_ = infiniteSearch;
}

int SearchOptions::depthLimit() const
{
    return depthLimit_;
}

void SearchOptions::setDepthLimit(int depthLimit)
{
    depthLimit_ = depthLimit;
}

int SearchOptions::nodeLimit() const
{
    return nodeLimit_;
}

void SearchOptions::setNodeLimit(int nodeLimit)
{
    nodeLimit_ = nodeLimit;
}

int SearchOptions::timeLimit() const
{
    return timeLimit_;
}

void SearchOptions::setTimeLimit(int timeLimit)
{
    timeLimit_ = timeLimit;
}

const std::vector<std::string> &SearchOptions::searchMoves() const
{
    return searchMoves_;
}

void SearchOptions::setSearchMoves(const std::vector<std::string> &searchMoves)
{
    searchMoves_ = searchMoves;
}

int SearchOptions::whiteTimeLeft() const
{
    return whiteTimeLeft_;
}

void SearchOptions::setWhiteTimeLeft(int whiteTimeLeft)
{
    whiteTimeLeft_ = whiteTimeLeft;
}

int SearchOptions::blackTimeLeft() const
{
    return blackTimeLeft_;
}

void SearchOptions::setBlackTimeLeft(int blackTimeLeft)
{
    blackTimeLeft_ = blackTimeLeft;
}

int SearchOptions::whiteIncrement() const
{
    return whiteIncrement_;
}

void SearchOptions::setWhiteIncrement(int whiteIncrement)
{
    whiteIncrement_ = whiteIncrement;
}

int SearchOptions::blackIncrement() const
{
    return blackIncrement_;
}

void SearchOptions::setBlackIncrement(int blackIncrement)
{
    blackIncrement_ = blackIncrement;
}

int SearchOptions::movesToGo() const
{
    return movesToGo_;
}

void SearchOptions::setMovesToGo(int movesToGo)
{
    movesToGo_ = movesToGo;
}
}
