#ifndef MOVEPICKER_H
#define MOVEPICKER_H

#include <vector>

#include "move.h"
#include "movecounter.h"
#include "position.h"
#include "searchdata.h"
#include "types.h"

namespace idun
{

class MovePicker
{
public:
    MovePicker(const Move &pvMove, const Position &position,
               const std::vector<Move> &moves, ThreadData &thread,
               PlyData *ply);

    bool hasNext() const;
    const Move &next();

private:
    unsigned long getMoveScore(const Position &position, const Move &move);

    const std::vector<Move> &moves_;
    ThreadData &thread_;

    struct IndexMap
    {
        unsigned index;
        unsigned long score;
    };

    std::vector<IndexMap> map_;

    unsigned current_;
};
}

#endif // MOVEPICKER_H
