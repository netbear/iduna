#include "move.h"

#include "bitboards.h"
#include <array>

namespace idun
{

Move::Move() : move_(0)
{
}

Move::Move(Square from, Square to, Move::Type type, PieceType promotionType)
{
    assert(promotionType != PieceType::NONE);
    assert(promotionType != PieceType::PAWN);
    assert(promotionType != PieceType::KING);

    move_ = static_cast<uint16_t>(to) | (static_cast<uint16_t>(from) << 6) |
            ((static_cast<uint16_t>(promotionType) - 1) << 12) |
            (static_cast<uint16_t>(type) << 14);
}

bool Move::isValid() const
{
    return destination() != origin();
}

Square Move::destination() const
{
    return static_cast<Square>(move_ & 0x003f);
}

Square Move::origin() const
{
    return static_cast<Square>((move_ >> 6) & 0x003f);
}

PieceType Move::promotionType() const
{
    return static_cast<PieceType>(((move_ >> 12) & 0x0003) + 1);
}

Move::Type Move::type() const
{
    return static_cast<Move::Type>((move_ >> 14) & 0x0003);
}

bool Move::operator==(const Move &move) const
{
    return move_ == move.move_;
}

bool Move::operator!=(const Move &move) const
{
    return !(*this == move);
}

std::ostream &operator<<(std::ostream &os, const Move &move)
{
    static constexpr std::array<char, 8> file{
        {'h', 'g', 'f', 'e', 'd', 'c', 'b', 'a'}};
    static constexpr std::array<char, 8> rank{
        {'1', '2', '3', '4', '5', '6', '7', '8'}};
    static const std::array<std::string, 8> piece{
        {"p", "n", "b", "r", "q", "k"}};

    if(move.isValid())
    {
        auto src = bb::getFileAndRank(move.origin());
        auto dest = bb::getFileAndRank(move.destination());

        os << file[static_cast<unsigned>(src.first)]
           << rank[static_cast<unsigned>(src.second)]
           << file[static_cast<unsigned>(dest.first)]
           << rank[static_cast<unsigned>(dest.second)];

        if(move.type() == Move::Type::PROMOTION)
        {
            os << piece[static_cast<unsigned>(move.promotionType())];
        }
    }
    else
    {
        os << "0000";
    }

    return os;
}
}
