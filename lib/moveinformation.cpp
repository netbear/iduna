#include "moveinformation.h"

namespace idun
{

MoveInformation::MoveInformation() = default;

Move MoveInformation::move() const
{
    return move_;
}

void MoveInformation::setMove(const Move &move)
{
    move_ = move;
}

Piece MoveInformation::capturedPiece() const
{
    return capturedPiece_;
}

void MoveInformation::setCapturedPiece(const Piece &capturedPiece)
{
    capturedPiece_ = capturedPiece;
}

Piece MoveInformation::movedPiece() const
{
    return movedPiece_;
}

void MoveInformation::setMovedPiece(const Piece &movedPiece)
{
    movedPiece_ = movedPiece;
}

Color MoveInformation::color() const
{
    return color_;
}

void MoveInformation::setColor(const Color &color)
{
    color_ = color;
}

Square MoveInformation::previousEnPassantSquare() const
{
    return previousEnPassantSquare_;
}

void MoveInformation::setPreviousEnPassantSquare(
    const Square &previousEnPassantSquare)
{
    previousEnPassantSquare_ = previousEnPassantSquare;
}

CastlingRights MoveInformation::previousCastlingRights() const
{
    return previousCastlingRights_;
}

void MoveInformation::setPreviousCastlingRights(
    const CastlingRights &previousCastlingRights)
{
    previousCastlingRights_ = previousCastlingRights;
}

unsigned MoveInformation::previousHalfMoveClock() const
{
    return previousHalfMoveClock_;
}

void MoveInformation::setPreviousHalfMoveClock(
    const unsigned &previousHalfMoveClock)
{
    previousHalfMoveClock_ = previousHalfMoveClock;
}

uint64_t MoveInformation::hash() const
{
    return hash_;
}

void MoveInformation::setHash(const uint64_t &hash)
{
    hash_ = hash;
}

bool MoveInformation::isCapture() const
{
    return capturedPiece().type() != PieceType::NONE;
}
}
