#ifndef TIMEMANAGER_H
#define TIMEMANAGER_H

#include "types.h"
#include <searchoptions.h>

namespace idun
{

class TimeManager
{
public:
    TimeManager();

    void init(const SearchOptions &searchOptions, Color activeColor,
              unsigned moveNumber);
    bool update(int16_t depth, long time);

    long depth() const;
    bool isTimeLimited() const;
    long time() const;
    long nodes() const;

private:
    enum class TimeControl
    {
        CONST_NODES,
        CONST_DEPTH,
        CONST_TIME,
        TIME,
        INFINITE
    };

    TimeControl type_;

    Color activeColor_;

    long depth_;
    long time_;
    long nodes_;
    long increment_;

    std::vector<long> history_;
};

} // namespace idun

#endif // TIMEMANAGER_H
