#ifndef PLYSTATS_H
#define PLYSTATS_H

#include "move.h"
#include "movecounter.h"
#include <array>
#include <limits>

namespace idun
{

struct ThreadData
{
    MoveCounter moveCounter;
    int16_t maxDepth = 0;
};

struct PlyData
{
    int16_t ply = 0;
    std::array<Move, 2> killerMoves;
};
}

#endif // PLYSTATS_H
