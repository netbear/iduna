#ifndef MOVEGENERATOR_H
#define MOVEGENERATOR_H

#include <ostream>
#include <vector>

#include "move.h"
#include "position.h"

namespace idun
{

class MoveGenerator
{
public:
    MoveGenerator();

    enum class MoveType
    {
        Normal = 0x01,
        Capture = 0x02,
        All = 0x03
    };

    std::vector<Move> generateLegalMoves(const Position &position,
                                         MoveType type = MoveType::All) const;
    std::vector<Move> generateMoves(const Position &position,
                                    MoveType type = MoveType::All) const;

    void generateMoves(const Position &position, std::vector<Move> &moves,
                       PieceType pieceType,
                       MoveType type = MoveType::All) const;
    void generatePawnMoves(const Position &position, std::vector<Move> &moves,
                           MoveType type = MoveType::All) const;
    void generateKnightMoves(const Position &position, std::vector<Move> &moves,
                             MoveType type = MoveType::All) const;
    void generateBishopMoves(const Position &position, std::vector<Move> &moves,
                             MoveType type = MoveType::All) const;
    void generateRookMoves(const Position &position, std::vector<Move> &moves,
                           MoveType type = MoveType::All) const;
    void generateQueenMoves(const Position &position, std::vector<Move> &moves,
                            MoveType type = MoveType::All) const;
    void generateKingMoves(const Position &position, std::vector<Move> &moves,
                           MoveType type = MoveType::All) const;

    Bitboard getPinnedPieces(const Position &position, Square targetSquare,
                             Color targetColor) const;

    bool isKingAttacked(const Position &position) const;
    bool isAttacked(const Position &position, Bitboard target,
                    Color attackingColor) const;
    bool isAttacked(const Position &position, Square targetSquare,
                    Color attackingColor) const;

    bool isLegal(const Position &position, const Move &move) const;

    enum class Notation
    {
        PureCoordinate,
        StandardAlgebraic,
        LongAlgebraic
    };
    std::ostream &getMoveRepresentation(std::ostream &stream,
                                        const Position &position,
                                        const Move &move,
                                        MoveGenerator::Notation notation);

    Move getMove(const Position &position, std::string stringMove) const;

private:
    bool isLegalNaive(const Position &position, const Move &move) const;
    bool aligned(Square from, Square intermediate, Square to) const;
    Bitboard xrayRookAttacks(Bitboard occupancy, Bitboard blockers,
                             Square rookSquare) const;
    Bitboard xrayBishopAttacks(Bitboard occupancy, Bitboard blockers,
                               Square bishopSquare) const;
};

template <> struct enable_enum_bitfield_operators<MoveGenerator::MoveType>
{
    static const bool enable = true;
};
}

#endif // MOVEGENERATOR_H
