#include "magic.h"

#include <algorithm>
#include <cstdlib>
#include <iomanip>
#include <random>

#include "bitboards.h"
#include "mask.h"

namespace idun
{

void Magic::init()
{
    Mask::init();

    if(!initialized_)
    {
        // Calculate size of attack table
        {
            unsigned n = 0;
            for(unsigned i = 0; i < 64; ++i)
            {
                n += (1ULL << bishopMagicNumbers_[i].bits);
                n += (1ULL << rookMagicNumbers_[i].bits);
            }
            attackTable_.reserve(n);
        }
        unsigned attackTablePos = 0;

        for(unsigned i = 0; i < 64; ++i)
        {
            SMagic &smagic = rookTable_[i];
            smagic.table = &attackTable_[attackTablePos];
            smagic.mask = occupancyMaskRook(static_cast<int>(i));
            smagic.magic = rookMagicNumbers_[i].number;
            smagic.shift = 64 - rookMagicNumbers_[i].bits;

            const uint64_t n = (1ULL << (64 - smagic.shift));
            for(unsigned j = 0; j < n; j++)
            {
                Bitboard occupancyVariation = occupancyVariations(
                    smagic.mask, rookMagicNumbers_[i].bits, j);
                uint64_t index = ((static_cast<uint64_t>(occupancyVariation) *
                                   smagic.magic) >>
                                  smagic.shift);
                smagic.table[index] =
                    moveBoardRook(occupancyVariation, static_cast<int>(i));
            }

            attackTablePos += n;
        }

        for(unsigned i = 0; i < 64; ++i)
        {
            SMagic &smagic = bishopTable_[i];
            smagic.table = &attackTable_[attackTablePos];
            smagic.mask = occupancyMaskBishop(static_cast<int>(i));
            smagic.magic = bishopMagicNumbers_[i].number;
            smagic.shift = 64 - bishopMagicNumbers_[i].bits;

            const uint64_t n = (1ULL << (64 - smagic.shift));
            for(unsigned j = 0; j < n; j++)
            {
                Bitboard occupancyVariation = occupancyVariations(
                    smagic.mask, bishopMagicNumbers_[i].bits, j);
                uint64_t index = ((static_cast<uint64_t>(occupancyVariation) *
                                   smagic.magic) >>
                                  smagic.shift);
                smagic.table[index] =
                    moveBoardBishop(occupancyVariation, static_cast<int>(i));
            }

            attackTablePos += n;
        }

        initialized_ = true;
    }
}

void Magic::findMagicNumbers(bool bishop)
{
    srandom(static_cast<unsigned>(time(nullptr)));

    std::cout << "std::array<Magic::MagicNumber, 64> Magic::"
              << (bishop ? "bishop" : "rook") << "MagicNumbers = {{"
              << std::endl;
    for(unsigned i = 0; i < 64; ++i)
    {

        unsigned bits =
            bishop ? bishopMagicNumbers_[i].bits : rookMagicNumbers_[i].bits;
        uint64_t number = findMagicNumber(static_cast<int>(i), bits, bishop);

        if((i + 1) % 4 == 1)
        {
            std::cout << "    ";
        }

        std::cout << "MagicNumber{0x" << std::hex << std::setw(16)
                  << std::setfill('0') << number << ", " << std::dec << bits
                  << "}, ";

        if((i + 1) % 4 == 0)
        {
            std::cout << std::endl;
        }
    }
    std::cout << "}};" << std::endl;
}

uint64_t Magic::findMagicNumber(int square, unsigned bits, bool bishop)
{
    auto random_uint64 = []() {
        uint64_t u1, u2, u3, u4;
        u1 = static_cast<uint64_t>(random()) & 0xFFFF;
        u2 = static_cast<uint64_t>(random()) & 0xFFFF;
        u3 = static_cast<uint64_t>(random()) & 0xFFFF;
        u4 = static_cast<uint64_t>(random()) & 0xFFFF;
        return u1 | (u2 << 16) | (u3 << 32) | (u4 << 48);
    };

    auto random_uint64_fewbits = [&random_uint64]() {
        return random_uint64() & random_uint64() & random_uint64();
    };

    Bitboard b[4096], a[4096], used[4096];

    Bitboard mask =
        bishop ? occupancyMaskBishop(square) : occupancyMaskRook(square);
    int n = bb::popCount(mask);

    for(unsigned i = 0; i < (1U << n); i++)
    {
        b[i] = occupancyVariations(mask, static_cast<unsigned>(n), i);
        a[i] = bishop ? attackSetBishop(b[i], square)
                      : attackSetRook(b[i], square);
    }

    for(int k = 0; k < 1000000000; k++)
    {
        uint64_t magic = random_uint64_fewbits();
        if(bb::popCount((static_cast<uint64_t>(mask) * magic) &
                        0xFF00000000000000ULL) < 6)
        {
            continue;
        }

        for(auto &v : used) {
            v = Mask::empty;
        }

        bool fail = false;
        for(int i = 0; !fail && i < (1 << n); i++)
        {
            uint64_t j = (static_cast<uint64_t>(b[i]) * magic) >> (64 - bits);

            if(used[j] == Mask::empty)
            {
                used[j] = a[i];
            }
            else if(used[j] != a[i])
            {
                fail = true;
            }
        }

        if(!fail)
        {
            return magic;
        }
    }
    return 0;
}

Bitboard Magic::getRookMoves(int square, Bitboard occupancyMask)
{
    return getMoves(rookTable_[static_cast<unsigned>(square)], occupancyMask);
}

Bitboard Magic::getBishopMoves(int square, Bitboard occupancyMask)
{
    return getMoves(bishopTable_[static_cast<unsigned>(square)], occupancyMask);
}

Bitboard Magic::getQueenMoves(int square, Bitboard occupancyMask)
{
    return getRookMoves(square, occupancyMask) |
           getBishopMoves(square, occupancyMask);
}

Bitboard Magic::getMoves(const Magic::SMagic &smagic, Bitboard occupancyMask)
{
    Bitboard *aptr = smagic.table;
    occupancyMask &= smagic.mask;
    occupancyMask *= Bitboard(smagic.magic);
    occupancyMask >>= smagic.shift;
    return aptr[static_cast<unsigned>(occupancyMask)];
}

constexpr Bitboard Magic::occupancyMaskRook(int square)
{
    auto fr = bb::getFileAndRank(square);
    int file = fr.first;
    int rank = fr.second;

    Bitboard mask = bb::getBitboard(file, 0) | bb::getBitboard(file, 7) |
                    bb::getBitboard(0, rank) | bb::getBitboard(7, rank) |
                    bb::getBitboard(square);

    return (Mask::file(file) | Mask::rank(rank)) & ~mask;
}

constexpr Bitboard Magic::occupancyMaskBishop(int square)
{
    auto fr = bb::getFileAndRank(square);
    int file = fr.first;
    int rank = fr.second;
    return (Mask::diagonal(file + rank) | Mask::antiDiagonal(7 - file + rank)) &
           Mask::center & ~bb::getBitboard(square);
}

constexpr Bitboard Magic::occupancyVariations(Bitboard occupancyMask,
                                              unsigned bits, unsigned index)
{
    Bitboard result = Mask::empty;
    for(unsigned i = 0; i < bits; ++i)
    {
        int j = bb::bitScanAndClear(occupancyMask);
        if((index & (1U << i)) != 0)
        {
            result |= Bitboard(1ULL << j);
        }
    }
    return result;
}

constexpr Bitboard Magic::moveBoard(Bitboard occupancyVariation, int square,
                                    const std::array<int8_t, 4> &shifts)
{
    Bitboard moveBoard(0x00);

    for(const auto &s : shifts)
    {
        Bitboard mask = bb::getBitboard(square);
        for(int i = 0; i < 7; ++i)
        {
            if(s < 0) {
                mask >>= -s;
            }
            else {
                mask <<= s;
            }

            moveBoard |= mask;
            if(occupancyVariation & mask)
            {
                break;
            }
        }
    }
    return moveBoard;
}

constexpr Bitboard Magic::moveBoardRook(Bitboard occupancyVariation, int square)
{
    Bitboard mask = (Mask::rank(square / 8) | Mask::file(square % 8));
    return moveBoard(occupancyVariation, square, {{-1, 1, -8, 8}}) & mask;
}

constexpr Bitboard Magic::moveBoardBishop(Bitboard occupancyVariation,
                                          int square)
{
    auto fr = bb::getFileAndRank(square);
    Bitboard mask = (Mask::diagonal(fr.first + fr.second) |
                     Mask::antiDiagonal(7 - fr.first + fr.second));
    return moveBoard(occupancyVariation, square, {{-7, 7, -9, 9}}) & mask;
}

constexpr Bitboard Magic::attackSet(Bitboard occupancyVariation, int square,
                                    const std::array<int8_t, 4> &shifts)
{
    Bitboard attackSet(0x00);

    for(const auto &s : shifts)
    {
        Bitboard end = Mask::empty;
        switch(s)
        {
            case 1:
                end = Bitboard(0x8080808080808080);
                break;
            case -1:
                end = Bitboard(0x0101010101010101);
                break;
            case 8:
                end = Bitboard(0xff00000000000000);
                break;
            case -8:
                end = Bitboard(0x00000000000000ff);
                break;
            case 9:
                end = Bitboard(0xff80808080808080);
                break;
            case 7:
                end = Bitboard(0xff01010101010101);
                break;
            case -9:
                end = Bitboard(0x01010101010101ff);
                break;
            case -7:
                end = Bitboard(0x80808080808080ff);
                break;
            default:
                assert(false);
        }

        Bitboard mask = bb::getBitboard(square);
        for(int i = 0; i < 7; ++i)
        {
            if(s < 0) {
                mask >>= -s;
            }
            else {
                mask <<= s;
            }

            if((occupancyVariation | end) & (mask))
            {
                attackSet |= mask;
                break;
            }
        }
    }
    return attackSet;
}

constexpr Bitboard Magic::attackSetRook(Bitboard occupancyVariation, int square)
{
    return attackSet(occupancyVariation, square, {{1, -1, 8, -8}});
}

constexpr Bitboard Magic::attackSetBishop(Bitboard occupancyVariation,
                                          int square)
{
    return attackSet(occupancyVariation, square, {{7, -7, 9, -9}});
}

bool Magic::initialized_ = false;

std::vector<Bitboard> Magic::attackTable_;
std::array<Magic::SMagic, 64> Magic::bishopTable_;
std::array<Magic::SMagic, 64> Magic::rookTable_;

std::array<Magic::MagicNumber, 64> Magic::rookMagicNumbers_ = {{
    MagicNumber{0x8080004000802011, 12}, MagicNumber{0x0240004020041000, 11},
    MagicNumber{0x2180100120000880, 11}, MagicNumber{0x8500042100100008, 11},
    MagicNumber{0x1100030008001004, 11}, MagicNumber{0x4100084241000400, 11},
    MagicNumber{0x020002000100b418, 11}, MagicNumber{0x0200020041022c84, 12},
    MagicNumber{0x1120800430400280, 11}, MagicNumber{0x1040400050002000, 10},
    MagicNumber{0x0100808010002000, 10}, MagicNumber{0x004a004012000a20, 10},
    MagicNumber{0x8010800800810400, 10}, MagicNumber{0x4402000200051008, 10},
    MagicNumber{0x4009002200140500, 10}, MagicNumber{0x0202000102004084, 11},
    MagicNumber{0x0080004020004000, 11}, MagicNumber{0x0090044020004008, 10},
    MagicNumber{0x2620004010080044, 10}, MagicNumber{0x5000808008001000, 10},
    MagicNumber{0x4000828008000400, 10}, MagicNumber{0x910e008100800400, 10},
    MagicNumber{0x04110c0010021108, 10}, MagicNumber{0x0005020008540481, 11},
    MagicNumber{0x0a90248280004000, 11}, MagicNumber{0x8440004080200081, 10},
    MagicNumber{0x3040200280100081, 10}, MagicNumber{0x1400480280100180, 10},
    MagicNumber{0x0012050100100800, 10}, MagicNumber{0x0802040080020080, 10},
    MagicNumber{0x6020420400210810, 10}, MagicNumber{0x4000030200164884, 11},
    MagicNumber{0x2080004000402000, 11}, MagicNumber{0x0400802000804002, 10},
    MagicNumber{0xa210c10293002000, 10}, MagicNumber{0x4800080080801000, 10},
    MagicNumber{0x0941000611000800, 10}, MagicNumber{0x0022800400800200, 10},
    MagicNumber{0x408a001412000841, 10}, MagicNumber{0x420101004a000094, 11},
    MagicNumber{0x0081400080a28000, 11}, MagicNumber{0x0500200040008080, 10},
    MagicNumber{0x6090102001010040, 10}, MagicNumber{0x0108002010010100, 10},
    MagicNumber{0x0008000400088080, 10}, MagicNumber{0x0042000408020010, 10},
    MagicNumber{0x0000020004010100, 10}, MagicNumber{0x0510308400520001, 11},
    MagicNumber{0x00110a0040208600, 11}, MagicNumber{0x0040802000400480, 10},
    MagicNumber{0x0000100020008080, 10}, MagicNumber{0x02020089a0401200, 10},
    MagicNumber{0x4100040080080080, 10}, MagicNumber{0x0000800400020080, 10},
    MagicNumber{0x0000100241480400, 10}, MagicNumber{0x0404498101440600, 11},
    MagicNumber{0x100011a142800301, 12}, MagicNumber{0x2020802040001101, 11},
    MagicNumber{0x08010014e0000941, 11}, MagicNumber{0x4060201000990055, 11},
    MagicNumber{0x000a002044101806, 11}, MagicNumber{0x9009000804000201, 11},
    MagicNumber{0x1000221801009004, 11}, MagicNumber{0x0001085024008102, 12},
}};

std::array<Magic::MagicNumber, 64> Magic::bishopMagicNumbers_ = {{
    MagicNumber{0x1884200842008010, 6}, MagicNumber{0x00204a0a00450400, 5},
    MagicNumber{0x201000c20e400a08, 5}, MagicNumber{0x8002208204a04008, 5},
    MagicNumber{0x010c104491800640, 5}, MagicNumber{0x00042a2030800522, 5},
    MagicNumber{0x0420882108220000, 5}, MagicNumber{0x8200804510a02003, 6},
    MagicNumber{0x041104a0a8010110, 5}, MagicNumber{0x2800900258204084, 5},
    MagicNumber{0x4000100130410100, 5}, MagicNumber{0x0204282041410020, 5},
    MagicNumber{0x0a1b111040010000, 5}, MagicNumber{0x8000010420841082, 5},
    MagicNumber{0x2010004210106810, 5}, MagicNumber{0x4001002104022000, 5},
    MagicNumber{0x0220054108010104, 5}, MagicNumber{0x0004102901280208, 5},
    MagicNumber{0x0001101001022300, 7}, MagicNumber{0x0080880802004200, 7},
    MagicNumber{0x0842800404a00410, 7}, MagicNumber{0x0003000200412400, 7},
    MagicNumber{0x0841088048086430, 5}, MagicNumber{0x0854400a02008440, 5},
    MagicNumber{0x0082428410040801, 5}, MagicNumber{0x2121500048621830, 5},
    MagicNumber{0xc018040002202600, 7}, MagicNumber{0x4022040020110020, 9},
    MagicNumber{0x0414040080410040, 9}, MagicNumber{0x8906120010880900, 7},
    MagicNumber{0x0001284101082802, 5}, MagicNumber{0x0800920200222202, 5},
    MagicNumber{0x1406300502400900, 5}, MagicNumber{0x0862482208204a00, 5},
    MagicNumber{0x4084002800840840, 7}, MagicNumber{0x2000040400280210, 9},
    MagicNumber{0x0800420020020081, 9}, MagicNumber{0x8010888100020108, 7},
    MagicNumber{0x0004080201005111, 5}, MagicNumber{0x8282008021020210, 5},
    MagicNumber{0x0080903032000808, 5}, MagicNumber{0x126c010110204800, 5},
    MagicNumber{0x02004a0290012200, 7}, MagicNumber{0x9120044010438200, 7},
    MagicNumber{0x408050a010400200, 7}, MagicNumber{0x0002100200204200, 7},
    MagicNumber{0x002004510a000640, 5}, MagicNumber{0x0248692100221208, 5},
    MagicNumber{0x0001080104202002, 5}, MagicNumber{0x4700804450040000, 5},
    MagicNumber{0x0010090088040800, 5}, MagicNumber{0x0200401904880048, 5},
    MagicNumber{0x02080004850400d0, 5}, MagicNumber{0x2000085010a98400, 5},
    MagicNumber{0x0060080210a40444, 5}, MagicNumber{0x0132089600920000, 5},
    MagicNumber{0x2100924410085802, 6}, MagicNumber{0x008001011d104200, 5},
    MagicNumber{0x210680c201208800, 5}, MagicNumber{0x0a2a000828420202, 5},
    MagicNumber{0x400020024008220c, 5}, MagicNumber{0x20400112a0011104, 5},
    MagicNumber{0x00044411a2080104, 5}, MagicNumber{0x9220045108130090, 6},
}};
}
