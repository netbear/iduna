#include "position.h"

#include <ostream>
#include <sstream>
#include <tuple>
#include <utility>

#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>

#include "bitboards.h"
#include "mask.h"
#include "zobrist.h"

namespace idun
{

Position::Position()
{
    Zobrist::init();
    Mask::init();
    clear();
}

Position::Position(const std::string &fen) : Position()
{
    set(fen);
}

void Position::doMove(Move move, MoveInformation *moveInformation)
{
    ++nodesVisited_;
    const Bitboard origin = bb::getBitboard(move.origin());
    const Bitboard destination = bb::getBitboard(move.destination());

    const Piece movedPiece = get(origin);
    Piece capturedPiece = get(destination);

    assert(movedPiece.color() == activeColor());
    assert((!capturedPiece.isValid()) ||
           (capturedPiece.color() != activeColor()));

    // Record move information
    moveInformation->setCapturedPiece(capturedPiece);
    moveInformation->setMovedPiece(movedPiece);
    moveInformation->setColor(activeColor_);
    moveInformation->setMove(move);
    moveInformation->setPreviousEnPassantSquare(enPassantSquare_);
    moveInformation->setPreviousCastlingRights(castlingRights_);
    moveInformation->setPreviousHalfMoveClock(halfMoveClock_);
    moveInformation->setHash(hash_);

    switch(move.type())
    {
        case Move::Type::EN_PASSANT:
        {
            // The caputred piece is not located at the destination square
            assert(!capturedPiece.isValid());

            Bitboard enPassantMask =
                whiteToMove() ? (destination >> 8) : (destination << 8);
            capturedPiece = Piece(PieceType::PAWN, invert(activeColor_));

            remove(capturedPiece, enPassantMask);
            remove(movedPiece, origin);
            put(movedPiece, destination);
            break;
        }
        case Move::Type::NORMAL:
            if(capturedPiece.isValid())
            {
                remove(capturedPiece, destination);
            }
            remove(movedPiece, origin);
            put(movedPiece, destination);
            break;
        case Move::Type::PROMOTION:
            if(capturedPiece.isValid())
            {
                remove(capturedPiece, destination);
            }
            remove(movedPiece, origin);
            put(Piece(move.promotionType(), activeColor_), destination);
            break;
        case Move::Type::CASTLING:
            // This can't be a caputring move
            assert(!capturedPiece.isValid());

            Bitboard kingDest, rookDest, rookOrigin;
            bool queenSide = destination > origin;
            if(queenSide)
            {
                kingDest = destination;
                rookDest = destination >> 1;
                rookOrigin = destination << 2;
            }
            else
            {
                kingDest = destination;
                rookDest = destination << 1;
                rookOrigin = destination >> 1;
            }

            assert(isEmpty(kingDest));
            assert(isEmpty(rookDest));

            // Move the rook
            Piece rookPiece(PieceType::ROOK, activeColor_);
            remove(rookPiece, rookOrigin);
            put(rookPiece, rookDest);

            // Move the king
            remove(movedPiece, origin);
            put(movedPiece, kingDest);
            break;
    }

    // If double pawn push
    const unsigned fromRank = whiteToMove() ? 1 : 6;
    const unsigned toRank = whiteToMove() ? 3 : 4;
    Square newEnPassantSquare = INVALID_SQUARE;
    if((movedPiece.type() == PieceType::PAWN) &&
       (Mask::rank(fromRank) & origin) && (Mask::rank(toRank) & destination))
    {
        Bitboard enPassantMask =
            whiteToMove() ? (destination >> 8) : (destination << 8);
        newEnPassantSquare = bb::getSquare(enPassantMask);
    }

    if(newEnPassantSquare != enPassantSquare_)
    {
        if(enPassantSquare_ != INVALID_SQUARE)
        {
            hash_ ^= Zobrist::enPassantFile(enPassantSquare_);
        }

        if(newEnPassantSquare != INVALID_SQUARE)
        {
            hash_ ^= Zobrist::enPassantFile(newEnPassantSquare);
        }

        enPassantSquare_ = newEnPassantSquare;
    }

    // Update castling rights
    CastlingRights newCastlingRights = castlingRights_;
    if((move.type() == Move::Type::CASTLING) ||
       (movedPiece.type() == PieceType::KING))
    {
        if(activeColor_ == Color::WHITE)
        {
            newCastlingRights &= ~(CastlingRights::WHITE_KING_SIDE |
                                   CastlingRights::WHITE_QUEEN_SIDE);
        }
        else
        {
            newCastlingRights &= ~(CastlingRights::BLACK_KING_SIDE |
                                   CastlingRights::BLACK_QUEEN_SIDE);
        }
    }
    else if(movedPiece.type() == PieceType::ROOK)
    {
        switch(static_cast<int>(move.origin()))
        {
            case 0:
                newCastlingRights &= ~(CastlingRights::WHITE_KING_SIDE);
                break;
            case 7:
                newCastlingRights &= ~(CastlingRights::WHITE_QUEEN_SIDE);
                break;
            case 56:
                newCastlingRights &= ~(CastlingRights::BLACK_KING_SIDE);
                break;
            case 63:
                newCastlingRights &= ~(CastlingRights::BLACK_QUEEN_SIDE);
                break;
        }
    }

    if(capturedPiece.type() == PieceType::ROOK)
    {
        switch(static_cast<int>(move.destination()))
        {
            case 0:
                newCastlingRights &= ~(CastlingRights::WHITE_KING_SIDE);
                break;
            case 7:
                newCastlingRights &= ~(CastlingRights::WHITE_QUEEN_SIDE);
                break;
            case 56:
                newCastlingRights &= ~(CastlingRights::BLACK_KING_SIDE);
                break;
            case 63:
                newCastlingRights &= ~(CastlingRights::BLACK_QUEEN_SIDE);
                break;
        }
    }

    if(newCastlingRights != castlingRights_)
    {
        hash_ ^= Zobrist::castling(static_cast<unsigned>(castlingRights_));
        hash_ ^= Zobrist::castling(static_cast<unsigned>(newCastlingRights));
        castlingRights_ = newCastlingRights;
    }

    // Update move counts
    if(movedPiece.type() == PieceType::PAWN ||
       capturedPiece.type() != PieceType::NONE)
    {
        halfMoveClock_ = 0;
    }
    else
    {
        ++halfMoveClock_;
    }

    if(activeColor_ == Color::BLACK)
    {
        ++fullMoveNumber_;
    }

    // Update active color
    hash_ ^= Zobrist::blackToMove();
    activeColor_ = invert(activeColor_);

    repetitionCounter_.push(hash());
}

void Position::undoMove(const MoveInformation *moveInfo)
{
    const Move move = moveInfo->move();
    const Piece capturedPiece = moveInfo->capturedPiece();
    const Piece movedPiece = moveInfo->movedPiece();

    const Bitboard origin = bb::getBitboard(move.origin());
    const Bitboard destination = bb::getBitboard(move.destination());

    assert(movedPiece.color() != activeColor());
    assert((!capturedPiece.isValid()) ||
           (capturedPiece.color() == activeColor()));

    switch(move.type())
    {
        case Move::Type::EN_PASSANT:
        {
            assert(!capturedPiece.isValid());
            const Bitboard enPassantMask =
                blackToMove() ? (destination >> 8) : (destination << 8);

            put(Piece(PieceType::PAWN, activeColor_), enPassantMask);
            put(movedPiece, origin);
            remove(movedPiece, destination);
            break;
        }
        case Move::Type::NORMAL:
            put(movedPiece, origin);
            remove(movedPiece, destination);
            if(capturedPiece.isValid())
            {
                put(capturedPiece, destination);
            }
            break;
        case Move::Type::PROMOTION:
            put(movedPiece, origin);
            remove(Piece(move.promotionType(), invert(activeColor_)),
                   destination);
            if(capturedPiece.isValid())
            {
                put(capturedPiece, destination);
            }
            break;
        case Move::Type::CASTLING:
            assert(!capturedPiece.isValid());

            Bitboard kingDest, rookDest, rookOrigin;
            bool queenSide = destination > origin;
            if(queenSide)
            {
                kingDest = destination;
                rookDest = destination >> 1;
                rookOrigin = destination << 2;
            }
            else
            {
                kingDest = destination;
                rookDest = destination << 1;
                rookOrigin = destination >> 1;
            }

            // Move the rook
            Piece rookPiece(PieceType::ROOK, invert(activeColor_));
            put(rookPiece, rookOrigin);
            remove(rookPiece, rookDest);

            // Move the king
            put(movedPiece, origin);
            remove(movedPiece, kingDest);

            assert(isEmpty(kingDest));
            assert(isEmpty(rookDest));
            break;
    }

    enPassantSquare_ = moveInfo->previousEnPassantSquare();
    castlingRights_ = moveInfo->previousCastlingRights();
    halfMoveClock_ = moveInfo->previousHalfMoveClock();

    if(activeColor_ == Color::WHITE)
    {
        --fullMoveNumber_;
    }

    hash_ = moveInfo->hash();
    activeColor_ = invert(activeColor_);

    repetitionCounter_.pop();
}

void Position::doNullMove(MoveInformation *moveInformation)
{
    moveInformation->setPreviousEnPassantSquare(enPassantSquare_);
    enPassantSquare_ = INVALID_SQUARE;

    // Switch side to move
    hash_ ^= Zobrist::blackToMove();
    activeColor_ = invert(activeColor_);
}

void Position::undoNullMove(const MoveInformation *moveInfo)
{
    enPassantSquare_ = moveInfo->previousEnPassantSquare();

    // Switch side to move
    hash_ ^= Zobrist::blackToMove();
    activeColor_ = invert(activeColor_);
}

void Position::clear()
{
    std::fill(typeBoard_.begin(), typeBoard_.end(), 0);
    std::fill(colorBoard_.begin(), colorBoard_.end(), 0);

    activeColor_ = Color::WHITE;
    castlingRights_ = CastlingRights::ALL;
    enPassantSquare_ = INVALID_SQUARE;
    halfMoveClock_ = 0;
    fullMoveNumber_ = 0;
    hash_ = 0;
    repetitionCounter_.clear();
    nodesVisited_ = 0;

    hash_ = 0;
}

void Position::set(const std::string &fen)
{
    clear();
    boost::tokenizer<boost::char_separator<char>> tokenizer(
        fen, boost::char_separator<char>(" "));

    // Placement
    auto token = tokenizer.begin();
    Square square = Square(63);
    for(auto c : *token)
    {
        if(c >= '0' && c <= '9')
        {
            square -= static_cast<Square>(c - '0');
            continue;
        }
        switch(c)
        {
            case 'P':
                put(Piece(PieceType::PAWN, Color::WHITE), square--);
                break;
            case 'N':
                put(Piece(PieceType::KNIGHT, Color::WHITE), square--);
                break;
            case 'B':
                put(Piece(PieceType::BISHOP, Color::WHITE), square--);
                break;
            case 'R':
                put(Piece(PieceType::ROOK, Color::WHITE), square--);
                break;
            case 'Q':
                put(Piece(PieceType::QUEEN, Color::WHITE), square--);
                break;
            case 'K':
                put(Piece(PieceType::KING, Color::WHITE), square--);
                break;
            case 'p':
                put(Piece(PieceType::PAWN, Color::BLACK), square--);
                break;
            case 'n':
                put(Piece(PieceType::KNIGHT, Color::BLACK), square--);
                break;
            case 'b':
                put(Piece(PieceType::BISHOP, Color::BLACK), square--);
                break;
            case 'r':
                put(Piece(PieceType::ROOK, Color::BLACK), square--);
                break;
            case 'q':
                put(Piece(PieceType::QUEEN, Color::BLACK), square--);
                break;
            case 'k':
                put(Piece(PieceType::KING, Color::BLACK), square--);
                break;
        }
    }

    // Active color
    ++token;
    if(*token == "w")
    {
        activeColor_ = Color::WHITE;
    }
    else if(*token == "b")
    {
        activeColor_ = Color::BLACK;
    }

    // Castling rights
    ++token;
    castlingRights_ = CastlingRights::NONE;
    for(auto c : *token)
    {
        switch(c)
        {
            case 'K':
                castlingRights_ |= CastlingRights::WHITE_KING_SIDE;
                break;
            case 'Q':
                castlingRights_ |= CastlingRights::WHITE_QUEEN_SIDE;
                break;
            case 'k':
                castlingRights_ |= CastlingRights::BLACK_KING_SIDE;
                break;
            case 'q':
                castlingRights_ |= CastlingRights::BLACK_QUEEN_SIDE;
                break;
        }
    }

    // Enpassant square
    ++token;
    if((*token)[0] == '-')
    {
        enPassantSquare_ = INVALID_SQUARE;
    }
    else
    {
        int file = (*token)[0] - 'a';
        int rank = (*token)[1] - '1';
        enPassantSquare_ = 8 * rank + (7 - file);
    }

    // Half move clock
    ++token;
    halfMoveClock_ = boost::lexical_cast<decltype(halfMoveClock_)>(*token);

    // Full move number
    ++token;
    fullMoveNumber_ = boost::lexical_cast<decltype(fullMoveNumber_)>(*token);

    hash_ = calculateHash();
}

std::string Position::fen() const
{
    const std::array<char, 6> symbolTable{{'P', 'N', 'B', 'R', 'Q', 'K'}};

    std::ostringstream ss;

    // Placement
    Square square = Square(63);
    int n = 0;
    while(square >= Square(0))
    {
        Piece piece = get(square);
        if(piece.type() == PieceType::NONE)
        {
            ++n;
        }
        else
        {
            if(n != 0)
            {
                ss << n;
                n = 0;
            }

            char symbol = symbolTable[static_cast<unsigned>(piece.type())] -
                          static_cast<char>(piece.color()) * ('A' - 'a');
            ss << symbol;
        }

        if(square % 8 == Square(0))
        {
            if(n != 0)
            {
                ss << n;
                n = 0;
            }

            if(square != Square(0))
            {
                ss << '/';
            }
        }

        --square;
    }

    // Color to move
    ss << ' ' << (activeColor_ == Color::WHITE ? 'w' : 'b');

    // Castling rights
    ss << ' ';

    if(castlingRights_ == CastlingRights::NONE)
    {
        ss << '-';
    }
    else
    {
        const std::array<char, 4> castlingSymbolTable{{'K', 'Q', 'k', 'q'}};
        for(unsigned i = 0; i < 4; ++i)
        {
            if((castlingRights_ & static_cast<CastlingRights>(1 << i)) !=
               CastlingRights::NONE)
            {
                ss << castlingSymbolTable[i];
            }
        }
    }

    // En passant
    ss << ' ';
    if(enPassantSquare_ == INVALID_SQUARE)
    {
        ss << '-';
    }
    else
    {
        int file = 7 - (static_cast<int>(enPassantSquare_) % 8);
        int rank = static_cast<int>(enPassantSquare_) / 8;
        ss << static_cast<char>('a' + file) << static_cast<char>('1' + rank);
    }

    // Half move clock
    ss << ' ' << halfMoveClock_;

    // Full move number
    ss << ' ' << fullMoveNumber_;

    return ss.str();
}

Color Position::activeColor() const
{
    return activeColor_;
}

Color Position::inactiveColor() const
{
    return activeColor_ == Color::WHITE ? Color::BLACK : Color::WHITE;
}

bool Position::whiteToMove() const
{
    return activeColor_ == Color::WHITE;
}

bool Position::blackToMove() const
{
    return activeColor_ == Color::BLACK;
}

CastlingRights Position::getCastlingRights() const
{
    return castlingRights_;
}

Square Position::getEnPassantSquare() const
{
    return enPassantSquare_;
}

unsigned Position::getFullMoveNumber() const
{
    return fullMoveNumber_;
}

unsigned Position::getHalfMoveClock() const
{
    return halfMoveClock_;
}

bool Position::operator==(const Position &position) const
{
    return typeBoard_ == position.typeBoard_ &&
           colorBoard_ == position.colorBoard_ &&
           activeColor_ == position.activeColor_ &&
           castlingRights_ == position.castlingRights_ &&
           enPassantSquare_ == position.enPassantSquare_ &&
           hash_ == position.hash_;
}

bool Position::operator!=(const Position &position) const
{
    return !(*this == position);
}

Piece Position::get(Bitboard mask) const
{
    assert(mask);
    Color color = Color::NONE;
    PieceType type = PieceType::NONE;

    for(size_t i = 0; i < colorBoard_.size(); ++i)
    {
        if(colorBoard_[i] & mask)
        {
            color = static_cast<Color>(i);
            break;
        }
    }

    if(color != Color::NONE)
    {
        for(size_t i = 0; i < typeBoard_.size(); ++i)
        {
            if(typeBoard_[i] & mask)
            {
                type = static_cast<PieceType>(i);
                break;
            }
        }
    }

    return Piece(type, color);
}

Piece Position::get(Square square) const
{
    assert(square >= Square(0) && square < Square(64));
    return get(bb::getBitboard(square));
}

void Position::put(Piece piece, Bitboard mask)
{
    assert(mask);
    typeBoard_[static_cast<unsigned>(piece.type())] |= mask;
    colorBoard_[static_cast<unsigned>(piece.color())] |= mask;

    // Update hash
    hash_ ^=
        Zobrist::pieceValue(piece.color(), piece.type(), bb::getSquare(mask));
}

void Position::put(Piece piece, Square square)
{
    assert(square >= Square(0) && square < Square(64));
    Bitboard mask = bb::getBitboard(square);
    put(piece, mask);
}

void Position::remove(Piece piece, Bitboard mask)
{
    assert(mask);
    typeBoard_[static_cast<unsigned>(piece.type())] &= ~mask;
    colorBoard_[static_cast<unsigned>(piece.color())] &= ~mask;

    // Update hash
    hash_ ^=
        Zobrist::pieceValue(piece.color(), piece.type(), bb::getSquare(mask));
}

void Position::remove(Piece piece, Square square)
{
    assert(square >= Square(0) && square < Square(64));
    Bitboard mask = bb::getBitboard(square);
    remove(piece, mask);
}

bool Position::isOccupied(Square square) const
{
    return isOccupied(bb::getBitboard(square));
}

bool Position::isOccupied(Bitboard bitboard) const
{
    return static_cast<bool>((colorBoard_[0] | colorBoard_[1]) & bitboard);
}

bool Position::isEmpty(Square square) const
{
    return !isOccupied(square);
}

bool Position::isEmpty(Bitboard bitboard) const
{
    return !isOccupied(bitboard);
}

Bitboard Position::getEmptyBitboard() const
{
    return ~getOccupiedBitboard();
}

Bitboard Position::getOccupiedBitboard() const
{
    return colorBoard_[0] | colorBoard_[1];
}

Bitboard Position::getFriendlyBitboard() const
{
    return colorBoard_[static_cast<unsigned>(activeColor())];
}

Bitboard Position::getHostileBitboard() const
{
    return colorBoard_[static_cast<unsigned>(inactiveColor())];
}

Bitboard Position::getBitboard(PieceType type) const
{
    return typeBoard_[static_cast<unsigned>(type)];
}

Bitboard Position::getBitboard(Color color) const
{
    return colorBoard_[static_cast<unsigned>(color)];
}

Bitboard Position::getBitboard(PieceType type, Color color) const
{
    return typeBoard_[static_cast<unsigned>(type)] &
           colorBoard_[static_cast<unsigned>(color)];
}

uint64_t Position::hash() const
{
    return hash_;
}

uint64_t Position::calculateHash() const
{
    uint64_t hash = 0;
    for(int s = 0; s < 64; ++s)
    {
        Piece piece = get(Square(s));

        if(piece.isValid())
        {
            hash ^= Zobrist::pieceValue(piece.color(), piece.type(), Square(s));
        }
    }

    if(activeColor() == Color::BLACK)
    {
        hash ^= Zobrist::blackToMove();
    }

    if(enPassantSquare_ != INVALID_SQUARE)
    {
        hash ^= Zobrist::enPassantFile(enPassantSquare_);
    }

    hash ^= Zobrist::castling(static_cast<unsigned>(castlingRights_));

    return hash;
}

unsigned Position::repetitions() const
{
    return repetitionCounter_.repetitionCount();
}

unsigned Position::nodesVisited() const
{
    return nodesVisited_;
}

std::ostream &operator<<(std::ostream &os, const Position &position)
{
    const std::array<std::array<char, 7>, 3> table{
        {{{'P', 'N', 'B', 'R', 'Q', 'K', ' '}},
         {{'p', 'n', 'b', 'r', 'q', 'k', ' '}},
         {{' ', ' ', ' ', ' ', ' ', ' ', ' '}}}};

    Square square(63);

    for(int y = 0; y < 8; ++y)
    {
        os << "   +---+---+---+---+---+---+---+---+\n";
        os << " " << 8 - y << " | ";

        for(int x = 0; x < 8; ++x)
        {
            Piece piece = position.get(square);
            os << table[static_cast<unsigned>(piece.color())]
                       [static_cast<unsigned>(piece.type())]
               << " | ";
            --square;
        }
        os << (7 - y) * 8 << "\n";
    }
    os << "   +---+---+---+---+---+---+---+---+\n";
    os << "     A   B   C   D   E   F   G   H  ";
    return os;
}
}
