#ifndef IDUN_MASK_H
#define IDUN_MASK_H

#include "bitboards.h"
#include "types.h"

#include <array>
#include <cassert>

namespace idun
{

class Mask
{
public:
    static void init();

    enum class File
    {
        A = 7,
        B = 6,
        C = 5,
        D = 4,
        E = 3,
        F = 2,
        G = 1,
        H = 0
    };

    enum class Quadrant
    {
        WHITE_KING = 0,
        WHITE_QUEEN = 1,
        BLACK_KING = 2,
        BLACK_QUEEN = 3
    };

    template <typename FromSquare, typename ToSquare>
    static Bitboard betweenExclusive(FromSquare from, ToSquare to)
    {
        assert(initialized_);
        assert(static_cast<unsigned>(from) >= 0 &&
               static_cast<unsigned>(from) < 64);
        assert(static_cast<unsigned>(to) >= 0 &&
               static_cast<unsigned>(to) < 64);
        return betweenExclusive_[static_cast<unsigned>(from)]
                                [static_cast<unsigned>(to)];
    }

    template <typename FromSquare, typename ToSquare>
    static Bitboard betweenInclusive(FromSquare from, ToSquare to)
    {
        assert(initialized_);
        assert(static_cast<unsigned>(from) >= 0 &&
               static_cast<unsigned>(from) < 64);
        assert(static_cast<unsigned>(to) >= 0 &&
               static_cast<unsigned>(to) < 64);
        return betweenInclusive_[static_cast<unsigned>(from)]
                                [static_cast<unsigned>(to)];
    }

    template <typename Rank> constexpr const static Bitboard rank(Rank rank)
    {
        return rank_[static_cast<unsigned>(rank)];
    }

    template <typename File> constexpr static Bitboard file(File file)
    {
        return file_[static_cast<unsigned>(file)];
    }

    template <typename File> constexpr static Bitboard rightOf(File file)
    {
        return rightOf_[static_cast<unsigned>(file)];
    }

    template <typename File> constexpr static Bitboard leftOf(File file)
    {
        return leftOf_[static_cast<unsigned>(file)];
    }

    template <typename File>
    constexpr static Bitboard neighboringFiles(File file)
    {
        return neighboringFiles_[static_cast<unsigned>(file)];
    }

    template <typename Square>
    constexpr static Bitboard knightMoves(Square square)
    {
        return knightMoves_[static_cast<unsigned>(square)];
    }

    template <typename Square>
    constexpr static Bitboard kingMoves(Square square)
    {
        return kingMoves_[static_cast<unsigned>(square)];
    }

    template <typename Square>
    constexpr static Bitboard pawnAttacks(Color color, Square square)
    {
        return pawnAttacks_[static_cast<unsigned>(color)]
                           [static_cast<unsigned>(square)];
    }

    template <typename DiagonalIndex>
    constexpr static Bitboard diagonal(DiagonalIndex index)
    {
        return diagonal_[static_cast<unsigned>(index)];
    }

    template <typename DiagonalIndex>
    constexpr static Bitboard antiDiagonal(DiagonalIndex index)
    {
        return antiDiagonal_[static_cast<unsigned>(index)];
    }

    constexpr static Bitboard castlingMask(Quadrant quadrant)
    {
        return castlingMask_[static_cast<unsigned>(quadrant)];
    }

    constexpr static Bitboard castlingMaskIncKing(Quadrant quadrant)
    {
        return castlingMaskIncKing_[static_cast<unsigned>(quadrant)];
    }

    static constexpr Bitboard empty = Bitboard(0x0000000000000000);
    static constexpr Bitboard center = Bitboard(0x007e7e7e7e7e7e00);

private:
    static bool initialized_;

    template <typename Array, typename Generator>
    static void generate_(Array &array, Generator &generator)
    {
        for(unsigned i = 0; i < array.size(); ++i)
        {
            array[i] = generator(i);
        }
    }

    constexpr static std::array<Bitboard, 8> rank_{
        {Bitboard(0x00000000000000ff), Bitboard(0x000000000000ff00),
         Bitboard(0x0000000000ff0000), Bitboard(0x00000000ff000000),
         Bitboard(0x000000ff00000000), Bitboard(0x0000ff0000000000),
         Bitboard(0x00ff000000000000), Bitboard(0xff00000000000000)}};

    constexpr static std::array<Bitboard, 8> file_{
        {Bitboard(0x0101010101010101), Bitboard(0x0202020202020202),
         Bitboard(0x0404040404040404), Bitboard(0x0808080808080808),
         Bitboard(0x1010101010101010), Bitboard(0x2020202020202020),
         Bitboard(0x4040404040404040), Bitboard(0x8080808080808080)}};

    static constexpr std::array<Bitboard, 8> rightOf_{{
        Bitboard(0x0000000000000000), Bitboard(0x0101010101010101),
        Bitboard(0x0303030303030303), Bitboard(0x0707070707070707),
        Bitboard(0x0f0f0f0f0f0f0f0f), Bitboard(0x1f1f1f1f1f1f1f1f),
        Bitboard(0x3f3f3f3f3f3f3f3f), Bitboard(0x7f7f7f7f7f7f7f7f),
    }};

    static constexpr std::array<Bitboard, 8> leftOf_{
        {Bitboard(0xfefefefefefefefe), Bitboard(0xfcfcfcfcfcfcfcfc),
         Bitboard(0xf8f8f8f8f8f8f8f8), Bitboard(0xf0f0f0f0f0f0f0f0),
         Bitboard(0xe0e0e0e0e0e0e0e0), Bitboard(0xc0c0c0c0c0c0c0c0),
         Bitboard(0x8080808080808080), Bitboard(0x0000000000000000)}};

    static constexpr std::array<Bitboard, 8> neighboringFiles_{
        {Bitboard(0x0202020202020202), Bitboard(0x0505050505050505),
         Bitboard(0x0a0a0a0a0a0a0a0a), Bitboard(0x1414141414141414),
         Bitboard(0x2828282828282828), Bitboard(0x5050505050505050),
         Bitboard(0xa0a0a0a0a0a0a0a0), Bitboard(0x4040404040404040)}};

    // index: file + rank
    static constexpr std::array<Bitboard, 15> diagonal_{{
        Bitboard(0x0000000000000001), Bitboard(0x0000000000000102),
        Bitboard(0x0000000000010204), Bitboard(0x0000000001020408),
        Bitboard(0x0000000102040810), Bitboard(0x0000010204081020),
        Bitboard(0x0001020408102040), Bitboard(0x0102040810204080),
        Bitboard(0x0204081020408000), Bitboard(0x0408102040800000),
        Bitboard(0x0810204080000000), Bitboard(0x1020408000000000),
        Bitboard(0x2040800000000000), Bitboard(0x4080000000000000),
        Bitboard(0x8000000000000000),
    }};

    // index: 7-file+rank
    static constexpr std::array<Bitboard, 15> antiDiagonal_{
        {Bitboard(0x0000000000000080), Bitboard(0x0000000000008040),
         Bitboard(0x0000000000804020), Bitboard(0x0000000080402010),
         Bitboard(0x0000008040201008), Bitboard(0x0000804020100804),
         Bitboard(0x0080402010080402), Bitboard(0x8040201008040201),
         Bitboard(0x4020100804020100), Bitboard(0x2010080402010000),
         Bitboard(0x1008040201000000), Bitboard(0x0804020100000000),
         Bitboard(0x0402010000000000), Bitboard(0x0201000000000000),
         Bitboard(0x0100000000000000)}};

    static constexpr std::array<Bitboard, 4> castlingMask_{{
        Bitboard(0x0000000000000006), // White king side
        Bitboard(0x0000000000000070), // White queen side
        Bitboard(0x0600000000000000), // Black king side
        Bitboard(0x7000000000000000)  // Black queen side
    }};

    static constexpr std::array<Bitboard, 4> castlingMaskIncKing_{{
        Bitboard(0x000000000000000E), // White king side
        Bitboard(0x0000000000000038), // White queen side
        Bitboard(0x0E00000000000000), // Black king side
        Bitboard(0x3800000000000000)  // Black queen side
    }};

    static std::array<std::array<Bitboard, 64>, 64> betweenExclusive_;
    static std::array<std::array<Bitboard, 64>, 64> betweenInclusive_;
    template <typename T>
    constexpr static Bitboard betweenGenerator_(T from, T to, bool inclusive)
    {
        Bitboard result = Mask::empty;
        auto a = bb::getFileAndRank(from);
        auto b = bb::getFileAndRank(to);

        Bitboard mask = bb::getBitboard(std::min(from, to));

        int startOffset = inclusive ? 0 : 1;
        int endOffset = inclusive ? 1 : 0;
        if(a.first == b.first)
        { // Same file
            for(int i = startOffset;
                i < std::abs(a.second - b.second) + endOffset; ++i)
            {
                result |= mask << i * 8;
            }
        }
        else if(a.second == b.second)
        { // Same rank
            for(int i = startOffset;
                i < std::abs(a.first - b.first) + endOffset; ++i)
            {
                result |= mask << i;
            }
        }
        else if(bb::getDiagonal(a.first, a.second) ==
                bb::getDiagonal(b.first, b.second))
        {
            for(int i = startOffset;
                i < std::abs(a.first - b.first) + endOffset; ++i)
            {
                result |= mask << i * 7;
            }
        }
        else if(bb::getAntiDiagonal(a.first, a.second) ==
                bb::getAntiDiagonal(b.first, b.second))
        {
            for(int i = startOffset;
                i < std::abs(a.first - b.first) + endOffset; ++i)
            {
                result |= mask << i * 9;
            }
        }

        return result;
    }

    static constexpr Bitboard notHFile_ = ~file_[static_cast<int>(File::H)];
    static constexpr Bitboard notGHFile_ =
        ~(file_[static_cast<int>(File::G)] | file_[static_cast<int>(File::H)]);
    static constexpr Bitboard notAFile_ = ~file_[static_cast<int>(File::A)];
    static constexpr Bitboard notABFile_ =
        ~(file_[static_cast<int>(File::A)] | file_[static_cast<int>(File::B)]);

    static std::array<Bitboard, 64> knightMoves_;
    constexpr static Bitboard knightMovesGenerator_(Square::base_type square)
    {
        Bitboard mask = bb::getBitboard(square);
        return (((mask >> 10) | (mask << 6)) & notABFile_) |
               (((mask >> 17) | (mask << 15)) & notAFile_) |
               (((mask >> 15) | (mask << 17)) & notHFile_) |
               (((mask >> 6) | (mask << 10)) & notGHFile_);
    }

    static std::array<Bitboard, 64> kingMoves_;
    constexpr static Bitboard kingMovesGenerator_(Square::base_type square)
    {
        Bitboard mask = bb::getBitboard(square);
        return (((mask >> 7) | (mask << 1) | (mask << 9)) & notHFile_) |
               (((mask >> 8) | (mask << 8))) |
               (((mask >> 9) | (mask >> 1) | (mask << 7)) & notAFile_);
    }

    static std::array<std::array<Bitboard, 64>, 2> pawnAttacks_;
    constexpr static Bitboard
    whitePawnAttacksGenerator_(Square::base_type square)
    {
        Bitboard mask = bb::getBitboard(square);
        return ((mask << 7) & notAFile_) | ((mask << 9) & notHFile_);
    }

    constexpr static Bitboard
    blackPawnAttacksGenerator_(Square::base_type square)
    {
        Bitboard mask = bb::getBitboard(square);
        return ((mask >> 7) & notHFile_) | ((mask >> 9) & notAFile_);
    }
};

} // namespace idun

#endif // IDUN_MASK_H
