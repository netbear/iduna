#include "repetitioncounter.h"

#include "position.h"

namespace idun
{

RepetitionCounter::RepetitionCounter()
{
    history_.reserve(50);
}

unsigned RepetitionCounter::repetitionCount() const
{
    if(history_.empty())
    {
        return 0;
    }

    uint64_t hash = history_.back();

    unsigned repCount = 1;
    auto lastIndex = static_cast<int>(history_.size()) - 1;
    for(int i = lastIndex - 2; i >= 0; i -= 2)
    {
        if(history_[static_cast<unsigned>(i)] == hash)
        {
            ++repCount;
        }
    }

    return repCount;
}

void RepetitionCounter::push(uint64_t hash)
{
    history_.push_back(hash);
}

void RepetitionCounter::pop()
{
    history_.pop_back();
}

void RepetitionCounter::clear()
{
    history_.clear();
}

} // namespace idun
