#include "movegenerator.h"

#include "bitboards.h"
#include "magic.h"
#include "mask.h"

#include <algorithm>

namespace idun
{

MoveGenerator::MoveGenerator()
{
    Mask::init();
    Magic::init();
}

std::vector<Move> MoveGenerator::generateLegalMoves(const Position &position,
                                                    MoveType type) const
{
    std::vector<Move> moves = generateMoves(position, type);
    auto s = std::partition(moves.begin(), moves.end(),
                            [this, &position](const Move &move) {
                                return isLegal(position, move);
                            });
    moves.erase(s, moves.end());
    return moves;
}

std::vector<Move> MoveGenerator::generateMoves(const Position &position,
                                               MoveType type) const
{
    std::vector<Move> moves;
    moves.reserve(32);

    generatePawnMoves(position, moves, type);
    generateKnightMoves(position, moves, type);
    generateBishopMoves(position, moves, type);
    generateRookMoves(position, moves, type);
    generateQueenMoves(position, moves, type);
    generateKingMoves(position, moves, type);

    return moves;
}

void MoveGenerator::generateMoves(const Position &position,
                                  std::vector<Move> &moves, PieceType pieceType,
                                  MoveType type) const
{
    switch(pieceType)
    {
        case PieceType::PAWN:
            generatePawnMoves(position, moves, type);
            break;
        case PieceType::KNIGHT:
            generateKnightMoves(position, moves, type);
            break;
        case PieceType::BISHOP:
            generateBishopMoves(position, moves, type);
            break;
        case PieceType::ROOK:
            generateRookMoves(position, moves, type);
            break;
        case PieceType::QUEEN:
            generateQueenMoves(position, moves, type);
            break;
        case PieceType::KING:
            generateKingMoves(position, moves, type);
            break;
        case PieceType::NONE:
            break;
    }
}

void MoveGenerator::generatePawnMoves(const Position &position,
                                      std::vector<Move> &moves,
                                      MoveType type) const
{
    Bitboard pawns =
        position.getBitboard(PieceType::PAWN, position.activeColor());
    Bitboard oponents = position.getBitboard(position.inactiveColor());
    Bitboard empty = position.getEmptyBitboard();

    auto insertMoves = [&moves, &position](Square origin, Square destination) {
        if((position.activeColor() == Color::WHITE &&
            (destination >= Square(56))) ||
           (position.activeColor() == Color::BLACK &&
            (destination < Square(8))))
        {
            // Promotion
            moves.emplace_back(Move(origin, destination, Move::Type::PROMOTION,
                                 PieceType::QUEEN));
            moves.emplace_back(Move(origin, destination, Move::Type::PROMOTION,
                                 PieceType::ROOK));
            moves.emplace_back(Move(origin, destination, Move::Type::PROMOTION,
                                 PieceType::BISHOP));
            moves.emplace_back(Move(origin, destination, Move::Type::PROMOTION,
                                 PieceType::KNIGHT));
        }
        else
        {
            moves.emplace_back(Move(origin, destination));
        }
    };

    if(static_cast<bool>(type & MoveType::Capture))
    {
        // Capture pawn moves
        constexpr Bitboard leftOfH = Mask::leftOf(Mask::File::H);
        constexpr Bitboard rightOfA = Mask::rightOf(Mask::File::A);

        // Insert ghost pawn to also generate en passant capture
        Square enPassantSquare = position.getEnPassantSquare();
        Bitboard enPassantMask = Mask::empty;
        if(enPassantSquare != INVALID_SQUARE)
        {
            enPassantMask = bb::getBitboard(enPassantSquare);
            oponents |= enPassantMask;
        }

        Bitboard captureLeftMask =
            oponents & (position.whiteToMove() ? ((pawns & rightOfA) << 9)
                                               : ((pawns & rightOfA) >> 7));
        Bitboard captureRightMask =
            oponents & (position.whiteToMove() ? ((pawns & leftOfH) << 7)
                                               : ((pawns & leftOfH) >> 9));

        while(captureLeftMask)
        {
            Square destination(bb::bitScanAndClear(captureLeftMask));
            Square origin = position.whiteToMove() ? destination - Square(9)
                                                   : destination + Square(7);
            if(destination != enPassantSquare)
            {
                insertMoves(origin, destination);
            }
            else
            {
                moves.emplace_back(
                    Move(origin, destination, Move::Type::EN_PASSANT));
            }
        }

        while(captureRightMask)
        {
            Square destination(bb::bitScanAndClear(captureRightMask));
            Square origin = position.whiteToMove() ? destination - Square(7)
                                                   : destination + Square(9);
            if(destination != enPassantSquare)
            {
                insertMoves(origin, destination);
            }
            else
            {
                moves.emplace_back(
                    Move(origin, destination, Move::Type::EN_PASSANT));
            }
        }
    }

    if(static_cast<bool>(type & MoveType::Normal))
    {
        // Normal pawn moves
        const Bitboard doubleMoveMask = position.whiteToMove()
                                            ? ((pawns & Mask::rank(1)) << 16)
                                            : ((pawns & Mask::rank(6)) >> 16);
        const Bitboard doubleEmptyMask = position.whiteToMove()
                                             ? ((empty & Mask::rank(2)) << 8)
                                             : ((empty & Mask::rank(5)) >> 8);
        Bitboard doubleMask = doubleMoveMask & doubleEmptyMask & empty;
        Bitboard singleMask =
            (position.whiteToMove() ? (pawns << 8) : (pawns >> 8)) & empty;

        while(doubleMask)
        {
            Square destination(bb::bitScanAndClear(doubleMask));
            Square delta(16);
            Square origin = position.whiteToMove() ? destination - delta
                                                   : destination + delta;
            insertMoves(origin, destination);
        }

        while(singleMask)
        {
            Square destination(bb::bitScanAndClear(singleMask));
            Square delta(8);
            Square origin = position.whiteToMove() ? destination - delta
                                                   : destination + delta;
            insertMoves(origin, destination);
        };
    }
}

void MoveGenerator::generateKnightMoves(const Position &position,
                                        std::vector<Move> &moves,
                                        MoveType type) const
{
    Bitboard knights =
        position.getBitboard(PieceType::KNIGHT, position.activeColor());
    Bitboard oponents = position.getBitboard(position.inactiveColor());
    Bitboard empty = position.getEmptyBitboard();

    while(knights)
    {
        int src = bb::bitScanAndClear(knights);

        if(static_cast<bool>(type & MoveType::Capture))
        {
            Bitboard captureMask = Mask::knightMoves(src) & oponents;
            while(captureMask)
            {
                int dst = bb::bitScanAndClear(captureMask);
                moves.emplace_back(Move(Square(src), Square(dst)));
            }
        }

        if(static_cast<bool>(type & MoveType::Normal))
        {
            Bitboard moveMask = Mask::knightMoves(src) & empty;
            while(moveMask)
            {
                int dst = bb::bitScanAndClear(moveMask);
                moves.emplace_back(Move(Square(src), Square(dst)));
            }
        }
    }
}

void MoveGenerator::generateBishopMoves(const Position &position,
                                        std::vector<Move> &moves,
                                        MoveType type) const
{
    Bitboard bishops =
        position.getBitboard(PieceType::BISHOP, position.activeColor());

    while(bishops)
    {
        int src = bb::bitScanAndClear(bishops);

        Bitboard mask =
            Magic::getBishopMoves(src, position.getOccupiedBitboard());

        if(static_cast<bool>(type & MoveType::Capture))
        {
            Bitboard captureMask = mask & position.getHostileBitboard();
            while(captureMask)
            {
                int dst = bb::bitScanAndClear(captureMask);
                moves.emplace_back(Move(Square(src), Square(dst)));
            }
        }

        if(static_cast<bool>(type & MoveType::Normal))
        {
            Bitboard moveMask = mask & position.getEmptyBitboard();
            while(moveMask)
            {
                int dst = bb::bitScanAndClear(moveMask);
                moves.emplace_back(Move(Square(src), Square(dst)));
            }
        }
    }
}

void MoveGenerator::generateRookMoves(const Position &position,
                                      std::vector<Move> &moves,
                                      MoveType type) const
{
    Bitboard rooks =
        position.getBitboard(PieceType::ROOK, position.activeColor());

    while(rooks)
    {
        int src = bb::bitScanAndClear(rooks);
        Bitboard mask =
            Magic::getRookMoves(src, position.getOccupiedBitboard());

        if(static_cast<bool>(type & MoveType::Capture))
        {
            Bitboard captureMask = mask & position.getHostileBitboard();
            while(captureMask)
            {
                int dst = bb::bitScanAndClear(captureMask);
                moves.emplace_back(Move(Square(src), Square(dst)));
            }
        }

        if(static_cast<bool>(type & MoveType::Normal))
        {
            Bitboard moveMask = mask & position.getEmptyBitboard();
            while(moveMask)
            {
                int dst = bb::bitScanAndClear(moveMask);
                moves.emplace_back(Move(Square(src), Square(dst)));
            }
        }
    }
}

void MoveGenerator::generateQueenMoves(const Position &position,
                                       std::vector<Move> &moves,
                                       MoveType type) const
{
    Bitboard queens =
        position.getBitboard(PieceType::QUEEN, position.activeColor());

    while(queens)
    {
        int src = bb::bitScanAndClear(queens);
        Bitboard mask =
            Magic::getQueenMoves(src, position.getOccupiedBitboard());

        if(static_cast<bool>(type & MoveType::Capture))
        {
            Bitboard captureMask = mask & position.getHostileBitboard();
            while(captureMask)
            {
                int dst = bb::bitScanAndClear(captureMask);
                moves.emplace_back(Move(Square(src), Square(dst)));
            }
        }

        if(static_cast<bool>(type & MoveType::Normal))
        {
            Bitboard moveMask = mask & position.getEmptyBitboard();
            while(moveMask)
            {
                int dst = bb::bitScanAndClear(moveMask);
                moves.emplace_back(Move(Square(src), Square(dst)));
            }
        }
    }
}

void MoveGenerator::generateKingMoves(const Position &position,
                                      std::vector<Move> &moves,
                                      MoveType type) const
{
    Bitboard kings =
        position.getBitboard(PieceType::KING, position.activeColor());
    Bitboard oponents = position.getBitboard(position.inactiveColor());
    Bitboard empty = position.getEmptyBitboard();
    Bitboard occupied = position.getOccupiedBitboard();

    while(kings)
    {
        int src = bb::bitScanAndClear(kings);

        if(static_cast<bool>(type & MoveType::Capture))
        {
            Bitboard captureMask = Mask::kingMoves(src) & oponents;
            while(captureMask)
            {
                int dst = bb::bitScanAndClear(captureMask);
                moves.emplace_back(Move(Square(src), Square(dst)));
            }
        }

        if(static_cast<bool>(type & MoveType::Normal))
        {
            // Castling moves
            auto rights = position.getCastlingRights();
            if(position.activeColor() == Color::WHITE)
            {
                if(static_cast<bool>(rights &
                                     CastlingRights::WHITE_KING_SIDE) &&
                   ((occupied &
                     Mask::castlingMask(Mask::Quadrant::WHITE_KING)) ==
                    Mask::empty))
                {
                    moves.emplace_back(
                        Move(Square(src), Square(1), Move::Type::CASTLING));
                }
                if(static_cast<bool>(rights &
                                     CastlingRights::WHITE_QUEEN_SIDE) &&
                   ((occupied &
                     Mask::castlingMask(Mask::Quadrant::WHITE_QUEEN)) ==
                    Mask::empty))
                {
                    moves.emplace_back(
                        Move(Square(src), Square(5), Move::Type::CASTLING));
                }
            }
            else
            {
                if(static_cast<bool>(rights &
                                     CastlingRights::BLACK_KING_SIDE) &&
                   ((occupied &
                     Mask::castlingMask(Mask::Quadrant::BLACK_KING)) ==
                    Mask::empty))
                {
                    moves.emplace_back(
                        Move(Square(src), Square(57), Move::Type::CASTLING));
                }
                if(static_cast<bool>(rights &
                                     CastlingRights::BLACK_QUEEN_SIDE) &&
                   ((occupied &
                     Mask::castlingMask(Mask::Quadrant::BLACK_QUEEN)) ==
                    Mask::empty))
                {
                    moves.emplace_back(
                        Move(Square(src), Square(61), Move::Type::CASTLING));
                }
            }

            Bitboard moveMask = Mask::kingMoves(src) & empty;
            while(moveMask)
            {
                int dst = bb::bitScanAndClear(moveMask);
                moves.emplace_back(Move(Square(src), Square(dst)));
            }
        }
    }
}

Bitboard MoveGenerator::getPinnedPieces(const Position &position,
                                        Square targetSquare,
                                        Color targetColor) const
{
    Color ownColor = targetColor;
    Color oponentColor = invert(targetColor);

    Bitboard ownPieces = position.getBitboard(ownColor);
    Bitboard oponentRQ = position.getBitboard(PieceType::ROOK, oponentColor) |
                         position.getBitboard(PieceType::QUEEN, oponentColor);
    Bitboard oponentBQ = position.getBitboard(PieceType::BISHOP, oponentColor) |
                         position.getBitboard(PieceType::QUEEN, oponentColor);

    Bitboard pinned = Mask::empty;

    Bitboard pinner = xrayRookAttacks(position.getOccupiedBitboard(), ownPieces,
                                      targetSquare) &
                      oponentRQ;
    while(pinner)
    {
        int sq = bb::bitScanAndClear(pinner);
        pinned |= Mask::betweenExclusive(sq, targetSquare) & ownPieces;
    }

    pinner = xrayBishopAttacks(position.getOccupiedBitboard(), ownPieces,
                               targetSquare) &
             oponentBQ;
    while(pinner)
    {
        int sq = bb::bitScanAndClear(pinner);
        pinned |= Mask::betweenExclusive(sq, targetSquare) & ownPieces;
    }

    return pinned;
}

bool MoveGenerator::isKingAttacked(const Position &position) const
{
    return isAttacked(
        position, position.getBitboard(PieceType::KING, position.activeColor()),
        position.inactiveColor());
}

bool MoveGenerator::isAttacked(const Position &position, Bitboard target,
                               Color attackingColor) const
{
    while(target != Mask::empty)
    {
        Square targetSquare = static_cast<Square>(bb::bitScanAndClear(target));

        bool result = isAttacked(position, targetSquare, attackingColor);
        if(result)
        {
            return true;
        }
    }
    return false;
}

bool MoveGenerator::isAttacked(const Position &position, Square targetSquare,
                               Color attackingColor) const
{
    Color attackedColor = invert(attackingColor);
    if(Mask::pawnAttacks(attackedColor, targetSquare) &
       position.getBitboard(PieceType::PAWN, attackingColor))
    {
        return true;
    }

    if(Mask::knightMoves(targetSquare) &
       position.getBitboard(PieceType::KNIGHT, attackingColor))
    {
        return true;
    }

    if(Mask::kingMoves(targetSquare) &
       position.getBitboard(PieceType::KING, attackingColor))
    {
        return true;
    }

    Bitboard rookMoves = Magic::getRookMoves(static_cast<int>(targetSquare),
                                             position.getOccupiedBitboard());
    if(rookMoves & (position.getBitboard(PieceType::ROOK, attackingColor) |
                    position.getBitboard(PieceType::QUEEN, attackingColor)))
    {
        return true;
    }

    Bitboard bishopMoves = Magic::getBishopMoves(
        static_cast<int>(targetSquare), position.getOccupiedBitboard());
    return static_cast<bool>(bishopMoves & (position.getBitboard(PieceType::BISHOP, attackingColor) |
                      position.getBitboard(PieceType::QUEEN, attackingColor)));
}

bool MoveGenerator::isLegal(const Position &position, const Move &move) const
{
    if(move.type() == Move::Type::CASTLING)
    {
        // Check squares for check
        bool valid = true;
        switch(static_cast<int>(move.destination()))
        {
            case 1:
                valid = !isAttacked(position, Mask::castlingMaskIncKing(
                                                  Mask::Quadrant::WHITE_KING),
                                    position.inactiveColor());
                break;
            case 5:
                valid = !isAttacked(position, Mask::castlingMaskIncKing(
                                                  Mask::Quadrant::WHITE_QUEEN),
                                    position.inactiveColor());
                break;
            case 57:
                valid = !isAttacked(position, Mask::castlingMaskIncKing(
                                                  Mask::Quadrant::BLACK_KING),
                                    position.inactiveColor());
                break;
            case 61:
                valid = !isAttacked(position, Mask::castlingMaskIncKing(
                                                  Mask::Quadrant::BLACK_QUEEN),
                                    position.inactiveColor());
                break;
        }
        if(!valid)
        {
            return false;
        }
    }

    if(move.type() == Move::Type::EN_PASSANT)
    {
        // Fallback to a naive move validator
        return isLegalNaive(position, move);
    }

    if(isKingAttacked(position))
    {
        // Fallback to a naive move validator
        return isLegalNaive(position, move);
    }

    Bitboard from = bb::getBitboard(move.origin());
    Color active = position.activeColor();

    auto piece = position.get(from);
    if(piece.type() == PieceType::KING)
    {
        // King move only legal if the destination square is not in check.
        Position p = position;
        MoveInformation mi;
        p.doMove(move, &mi);

        // TODO: Modify the isAttacked to take the bitboards and not the
        // position.
        if(isAttacked(p, p.getBitboard(PieceType::KING, p.inactiveColor()),
                      p.activeColor()))
        {
            return false;
        }
    }
    else
    {
        // An absolute pinned piece is only allowed to move along the pinning
        // direction.
        Bitboard ownKingBitboard =
            position.getBitboard(PieceType::KING, active);
        int kingSquare = bb::bitScan(ownKingBitboard);
        Bitboard pinned = getPinnedPieces(position, Square(kingSquare), active);

        return !(pinned & from) ||
               aligned(move.origin(), move.destination(), Square(kingSquare));
    }

    return true;
}

std::ostream &
MoveGenerator::getMoveRepresentation(std::ostream &stream,
                                     const Position &position, const Move &move,
                                     MoveGenerator::Notation notation)
{
    static constexpr std::array<char, 8> fileSymbol{
        {'h', 'g', 'f', 'e', 'd', 'c', 'b', 'a'}};
    static constexpr std::array<char, 8> rankSymbol{
        {'1', '2', '3', '4', '5', '6', '7', '8'}};
    static const std::array<std::string, 6> pieceSymbol{
        {"", "N", "B", "R", "Q", "K"}};

    switch(notation)
    {
        case Notation::PureCoordinate:
        {
            stream << move;
            break;
        }
        case Notation::LongAlgebraic:
        {
            auto piece = position.get(move.origin());
            auto destPiece = position.get(move.destination());
            auto origin = bb::getFileAndRank(move.origin());
            auto dest = bb::getFileAndRank(move.destination());

            stream << pieceSymbol[static_cast<unsigned>(piece.type())]
                   << fileSymbol[static_cast<unsigned>(origin.first)]
                   << rankSymbol[static_cast<unsigned>(origin.second)]
                   << (destPiece.isValid() ? 'x' : '-')
                   << fileSymbol[static_cast<unsigned>(dest.first)]
                   << rankSymbol[static_cast<unsigned>(dest.second)];

            if(move.type() == Move::Type::PROMOTION)
            {
                stream
                    << pieceSymbol[static_cast<unsigned>(move.promotionType())];
            }

            break;
        }
        case Notation::StandardAlgebraic:
        {
            if(move.type() == Move::Type::CASTLING)
            {
                auto file = bb::getFile(move.destination());
                if(file == 1)
                {
                    stream << "O-O";
                }
                else if(file == 5)
                {
                    stream << "O-O-O";
                }
            }
            else
            {
                auto piece = position.get(move.origin());

                // Piece symbol
                stream << pieceSymbol[static_cast<unsigned>(piece.type())];

                // Get psuedo legal moves for given piece
                std::vector<Move> moves;
                generateMoves(position, moves, piece.type());

                // Can more than one piece move to the destination square?
                auto destinationPredicate = [&move](const Move &m) {
                    return move.destination() == m.destination();
                };
                auto partIt = std::partition(std::begin(moves), std::end(moves),
                                             destinationPredicate);
                auto n = std::distance(std::begin(moves), partIt);

                assert(n > 0);
                if(n > 1)
                {
                    // Yes, we need to include from square information.

                    // In order of priority
                    // 1. file of departure if different
                    // 2. rank of departure if the files are the same but the
                    // ranks differ
                    // 3. the complete origin square coordinate otherwise

                    auto origin = bb::getFileAndRank(move.origin());

                    auto fileCount = std::count_if(
                        std::begin(moves), partIt, [&origin](const Move &m) {
                            return origin.first == bb::getFile(m.origin());
                        });
                    auto rankCount = std::count_if(
                        std::begin(moves), partIt, [&origin](const Move &m) {
                            return origin.second == bb::getRank(m.origin());
                        });
                    if(fileCount == 1)
                    {
                        stream
                            << fileSymbol[static_cast<unsigned>(origin.first)];
                    }
                    else if(rankCount == 1)
                    {
                        stream
                            << rankSymbol[static_cast<unsigned>(origin.second)];
                    }
                    else
                    {
                        stream
                            << fileSymbol[static_cast<unsigned>(origin.first)]
                            << rankSymbol[static_cast<unsigned>(origin.second)];
                    }
                }

                // Indicate if this is a capture move
                auto capturedPiece = position.get(move.destination());
                if(capturedPiece.isValid())
                {
                    stream << "x";
                }

                // Destination square
                auto dest = bb::getFileAndRank(move.destination());
                stream << fileSymbol[static_cast<unsigned>(dest.first)]
                       << rankSymbol[static_cast<unsigned>(dest.second)];

                // If promotion
                if(move.type() == Move::Type::PROMOTION)
                {
                    stream << "=" << pieceSymbol[static_cast<unsigned>(
                                         move.promotionType())];
                }
            }

            break;
        }
    }

    return stream;
}

Move MoveGenerator::getMove(const Position &position,
                            std::string stringMove) const
{
    assert(stringMove.length() >= 4);

    // Decode
    int fromFile = stringMove[0] - 'a';
    int fromRank = stringMove[1] - '1';
    int toFile = stringMove[2] - 'a';
    int toRank = stringMove[3] - '1';

    assert(fromFile >= 0 && fromFile <= 7);
    assert(fromRank >= 0 && fromRank <= 7);
    assert(toFile >= 0 && toFile <= 7);
    assert(toRank >= 0 && toRank <= 7);

    // Calculate squares
    Square origin(fromRank * 8 + 7 - fromFile);
    Square destination(toRank * 8 + 7 - toFile);

    // Check if promotion
    PieceType promotionPiece = PieceType::NONE;
    if(stringMove.length() > 4)
    {
        switch(stringMove[4])
        {
            case 'q':
                promotionPiece = PieceType::QUEEN;
                break;
            case 'r':
                promotionPiece = PieceType::ROOK;
                break;
            case 'b':
                promotionPiece = PieceType::BISHOP;
                break;
            case 'n':
                promotionPiece = PieceType::KNIGHT;
                break;
        }
    }

    bool promotion = promotionPiece != PieceType::NONE;

    // Compare to legal moves.
    // TODO: If need be this could be optimized, no need to generate all legal
    // moves.
    auto legalMoves = generateLegalMoves(position);
    Move move;
    for(const auto &m : legalMoves)
    {
        if(m.origin() == origin && m.destination() == destination &&
           ((!promotion) ||
            (m.promotionType() ==
             promotionPiece))) // TODO: Verify if this is correct.
        {
            move = m;
        }
    }
    return move;
}

bool MoveGenerator::isLegalNaive(const Position &position,
                                 const Move &move) const
{
    // Do move
    Position p = position;
    MoveInformation mi;
    p.doMove(move, &mi);

    bool attacked = isAttacked(
        p, p.getBitboard(PieceType::KING, p.inactiveColor()), p.activeColor());

    return !attacked;
}

bool MoveGenerator::aligned(Square from, Square intermediate, Square to) const
{
    Square min = std::min({from, to, intermediate});
    Square max = std::max({from, to, intermediate});

    Bitboard mask = bb::getBitboard(from) | bb::getBitboard(intermediate) |
                    bb::getBitboard(to);
    return (Mask::betweenInclusive(min, max) & mask) == mask;
}

Bitboard MoveGenerator::xrayRookAttacks(Bitboard occupancy, Bitboard blockers,
                                        Square rookSquare) const
{
    Bitboard attackers =
        Magic::getRookMoves(static_cast<int>(rookSquare), occupancy);
    blockers &= attackers;
    return attackers ^ Magic::getRookMoves(static_cast<int>(rookSquare),
                                           occupancy ^ blockers);
}

Bitboard MoveGenerator::xrayBishopAttacks(Bitboard occupancy, Bitboard blockers,
                                          Square bishopSquare) const
{
    Bitboard attackers =
        Magic::getBishopMoves(static_cast<int>(bishopSquare), occupancy);
    blockers &= attackers;
    return attackers ^ Magic::getBishopMoves(static_cast<int>(bishopSquare),
                                             occupancy ^ blockers);
}
}
