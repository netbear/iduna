#ifndef EVALUATOR_H
#define EVALUATOR_H

#include "position.h"
#include "types.h"
#include <array>

namespace idun
{

class Evaluator
{
public:
    Evaluator();

    Score evaluate(const Position &position) const;

    Score pieceValue(PieceType pieceType) const;

    enum class GameState
    {
        Opening,
        Middle,
        End
    };

    GameState getGameState(const Position &position) const;

private:
    using SquareTable = std::array<Score, 64>;

    Score calculate(Bitboard pieces, SquareTable table, Score pieceValue,
                    Color color) const;

    constexpr static std::array<Score, 7> pieceValue_{
        {100, 320, 330, 500, 900, 20000, 0}};

    // clang-format off
    constexpr static std::array<Score, 64> pawnSquareTable
    {{
        0,  0,  0,  0,  0,  0,  0,  0,
       50, 50, 50, 50, 50, 50, 50, 50,
       10, 10, 20, 30, 30, 20, 10, 10,
        5,  5, 10, 25, 25, 10,  5,  5,
        0,  0,  0, 20, 20,  0,  0,  0,
        5, -5,-10,  0,  0,-10, -5,  5,
        5, 10, 10,-20,-20, 10, 10,  5,
        0,  0,  0,  0,  0,  0,  0,  0
    }};

    constexpr static std::array<Score, 64> knightSquareTable
    {{
        -50,-40,-30,-30,-30,-30,-40,-50,
        -40,-20,  0,  0,  0,  0,-20,-40,
        -30,  0, 10, 15, 15, 10,  0,-30,
        -30,  5, 15, 20, 20, 15,  5,-30,
        -30,  0, 15, 20, 20, 15,  0,-30,
        -30,  5, 10, 15, 15, 10,  5,-30,
        -40,-20,  0,  5,  5,  0,-20,-40,
        -50,-40,-30,-30,-30,-30,-40,-50,
    }};

    constexpr static std::array<Score, 64> bishopSquareTable
    {{
        -20,-10,-10,-10,-10,-10,-10,-20,
        -10,  0,  0,  0,  0,  0,  0,-10,
        -10,  0,  5, 10, 10,  5,  0,-10,
        -10,  5,  5, 10, 10,  5,  5,-10,
        -10,  0, 10, 10, 10, 10,  0,-10,
        -10, 10, 10, 10, 10, 10, 10,-10,
        -10,  5,  0,  0,  0,  0,  5,-10,
        -20,-10,-10,-10,-10,-10,-10,-20,
    }};

    constexpr static std::array<Score, 64> rookSquareTable
    {{
        0,  0,  0,  0,  0,  0,  0,  0,
        5, 10, 10, 10, 10, 10, 10,  5,
       -5,  0,  0,  0,  0,  0,  0, -5,
       -5,  0,  0,  0,  0,  0,  0, -5,
       -5,  0,  0,  0,  0,  0,  0, -5,
       -5,  0,  0,  0,  0,  0,  0, -5,
       -5,  0,  0,  0,  0,  0,  0, -5,
        0,  0,  0,  5,  5,  0,  0,  0
    }};

    constexpr static std::array<Score, 64> queenSquareTable
    {{
        -20,-10,-10, -5, -5,-10,-10,-20,
        -10,  0,  0,  0,  0,  0,  0,-10,
        -10,  0,  5,  5,  5,  5,  0,-10,
         -5,  0,  5,  5,  5,  5,  0, -5,
          0,  0,  5,  5,  5,  5,  0, -5,
        -10,  5,  5,  5,  5,  5,  0,-10,
        -10,  0,  5,  0,  0,  0,  0,-10,
        -20,-10,-10, -5, -5,-10,-10,-20
    }};

    constexpr static std::array<Score, 64> kingSquareTable
    {{
        -30,-40,-40,-50,-50,-40,-40,-30,
        -30,-40,-40,-50,-50,-40,-40,-30,
        -30,-40,-40,-50,-50,-40,-40,-30,
        -30,-40,-40,-50,-50,-40,-40,-30,
        -20,-30,-30,-40,-40,-30,-30,-20,
        -10,-20,-20,-20,-20,-20,-20,-10,
         20, 20,  0,  0,  0,  0, 20, 20,
         20, 30, 10,  0,  0, 10, 30, 20
    }};

    constexpr static std::array<Score, 64> kingEndSquareTable
    {{
        -50,-40,-30,-20,-20,-30,-40,-50,
        -30,-20,-10,  0,  0,-10,-20,-30,
        -30,-10, 20, 30, 30, 20,-10,-30,
        -30,-10, 30, 40, 40, 30,-10,-30,
        -30,-10, 30, 40, 40, 30,-10,-30,
        -30,-10, 20, 30, 30, 20,-10,-30,
        -30,-30,  0,  0,  0,  0,-30,-30,
        -50,-30,-30,-30,-30,-30,-30,-50
    }};
    // clang-format on

    constexpr static unsigned squareIndex(Color color, int index)
    {
        assert(index >= 0 && index < 64);
        return color == Color::WHITE ? 63 - static_cast<unsigned>(index)
                                     : static_cast<unsigned>(index);
    }

    constexpr static std::array<std::array<Score, 64>, 6> pieceSquareTable{
        {pawnSquareTable, knightSquareTable, bishopSquareTable, rookSquareTable,
         queenSquareTable, kingSquareTable}};

    constexpr static std::array<std::array<Score, 64>, 6>
        pieceSquareTableEndgame{{pawnSquareTable, knightSquareTable,
                                 bishopSquareTable, rookSquareTable,
                                 queenSquareTable, kingEndSquareTable}};
};
}

#endif // EVALUATOR_H
