#ifndef MOVE_H
#define MOVE_H

#include <cinttypes>
#include <iostream>

#include "types.h"

namespace idun
{

class Move
{
public:
    enum class Type
    {
        NORMAL = 0,
        PROMOTION = 1,
        EN_PASSANT = 2,
        CASTLING = 3
    };

    Move();
    Move(Square from, Square to, Type type = Type::NORMAL,
         PieceType promotionType = PieceType::KNIGHT);

    bool isValid() const;

    Square destination() const;
    Square origin() const;
    PieceType promotionType() const;
    Type type() const;

    bool operator==(const Move &move) const;
    bool operator!=(const Move &move) const;

private:
    /* Move format (same as stockfish)
     * 0-5, destination square
     * 6-11, origin square
     * 12-13, promotion piece type (PieceType-1)
     * 14-15, move type, normal (0), promotion (1), en passant (2), castling(3)
     */
    uint16_t move_;
};

std::ostream &operator<<(std::ostream &os, const Move &move);
}

#endif // MOVE_H
