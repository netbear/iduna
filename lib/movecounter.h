#ifndef IDUN_MOVECOUNTER_H
#define IDUN_MOVECOUNTER_H

#include "move.h"
#include "piece.h"
#include <array>
#include <vector>

namespace idun
{

class MoveCounter
{
public:
    MoveCounter();

    void reset();
    void count(idun::Move move, Piece movedPiece, unsigned long increment = 1);
    unsigned long getCount(idun::Move move, Piece movedPiece) const;
    double getNormalizedCount(idun::Move move, Piece movedPiece) const;

private:
    std::array<std::array<unsigned long, 6 * 2>, 64> counter_;
    unsigned long total_;
};

} // namespace idun

#endif // IDUN_MOVECOUNTER_H
