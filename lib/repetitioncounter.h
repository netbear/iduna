#ifndef REPETITIONCOUNTER_H
#define REPETITIONCOUNTER_H

#include <cinttypes>
#include <vector>

namespace idun
{

class RepetitionCounter
{
public:
    RepetitionCounter();

    unsigned repetitionCount() const;

    void push(uint64_t hash);
    void pop();

    void clear();

private:
    // TODO: Possibly replace with an array since we have the
    // 50 move rule this list should not be able to be longer
    // than 50
    std::vector<uint64_t> history_;
};

} // namespace idun

#endif // REPETITIONCOUNTER_H
