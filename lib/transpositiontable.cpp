#include "transpositiontable.h"

#include <cmath>

namespace idun
{

TranspositionTable::TranspositionTable()
{
    resize(1);
}

const TranspositionTable::Entry
TranspositionTable::lookup(TranspositionTable::Key key) const
{
    const auto maskedKey = key & indexMask_;
    const Entry entry = table_[maskedKey];

    if(entry.key() == key)
    {
        return entry;
    }

    return Entry();
}

void TranspositionTable::store(TranspositionTable::Entry entry)
{
    const auto maskedKey = entry.key() & indexMask_;
    table_[maskedKey] = entry;
}

void TranspositionTable::clear()
{
    std::fill(table_.begin(), table_.end(), Entry());
}

void TranspositionTable::resize(unsigned mib)
{
    size_ = std::pow(
        2, std::floor(std::log(mib * 1048576 / sizeof(Entry)) / std::log(2)));

    indexMask_ = size_ - 1;

    table_.resize(size_);
    table_.shrink_to_fit();
}

uint64_t TranspositionTable::size() const
{
    return size_;
}

} // namespace idun
