#include "movepicker.h"

#include <algorithm>

namespace idun
{

MovePicker::MovePicker(const Move &pvMove, const Position &position,
                       const std::vector<Move> &moves, ThreadData &thread,
                       PlyData *ply)
    : moves_(moves), thread_(thread), current_(0)
{
    map_.reserve(moves_.size());

    for(unsigned i = 0; i < moves_.size(); ++i)
    {
        const Move &move = moves_[i];
        assert(move.isValid());

        if(move == pvMove)
        {
            map_.push_back({i, std::numeric_limits<unsigned long>::max()});
        }
        else if(move == ply->killerMoves[0])
        {
            map_.push_back({i, std::numeric_limits<unsigned long>::max() - 1});
        }
        else if(move == ply->killerMoves[1])
        {
            map_.push_back({i, std::numeric_limits<unsigned long>::max() - 2});
        }
        else
        {
            map_.push_back({i, getMoveScore(position, move)});
        }
    }

    std::sort(map_.begin(), map_.end(),
              [](const IndexMap &si0, const IndexMap &si1) {
                  return si0.score > si1.score;
              });
}

bool MovePicker::hasNext() const
{
    assert(map_.size() == moves_.size());
    return current_ < map_.size();
}

const Move &MovePicker::next()
{
    assert(map_.size() == moves_.size());
    assert(current_ < map_.size());
    auto &move = moves_[map_[current_++].index];
    assert(move.isValid());
    return move;
}

unsigned long MovePicker::getMoveScore(const Position &position,
                                       const Move &move)
{

    auto movedPiece = position.get(move.origin());
    auto capturedPiece = position.get(move.destination());

    switch(capturedPiece.type())
    {
        case PieceType::NONE:
            // Non capture order according to beta cut-off count.
            // Range: [0, 999999];
            return static_cast<unsigned long>(
                thread_.moveCounter.getCount(move, movedPiece));
        default:
            // Capture move order mvv-lva (Most Valuable Victim - Least Valuable
            // Atacker)
            // Range: [1000000, 1000055]
            return 10 * static_cast<unsigned long>(capturedPiece.type()) +
                   (static_cast<unsigned long>(PieceType::KING) -
                    static_cast<unsigned long>(movedPiece.type())) +
                   1000000;
    }
}
}
