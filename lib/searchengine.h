#ifndef SEARCHENGINE_H
#define SEARCHENGINE_H

#include <boost/signals2.hpp>
#include <vector>

#include "position.h"
#include "types.h"

#include "evaluator.h"
#include "movecounter.h"
#include "movegenerator.h"
#include "movepicker.h"
#include "searchdata.h"
#include "searchoptions.h"
#include "threadpool.h"
#include "timemanager.h"
#include "transpositiontable.h"

namespace idun
{

class SearchEngine
{
public:
    struct PrincipalVariation
    {

        enum class ScoreType
        {
            CentiPawns,
            Mate,
            LowerBound,
            UpperBound
        };

        long time;
        int16_t depth;
        int16_t selectiveDepth;
        ScoreType scoreType;
        Score score;
        unsigned nodes;
        unsigned nodesPerSecond;
        std::vector<Move> moves;
    };

    SearchEngine();
    ~SearchEngine();

    typedef boost::signals2::signal<void(PrincipalVariation)> InfoSignal;
    boost::signals2::connection
    subscribeInfo(const InfoSignal::slot_type &subscriber);

    void newGame();

    Move run(Position position, const SearchOptions &options,
             std::atomic<bool> &running);

    /**
     * @brief Set the size of the transposition table in MiB.
     * @param size Size in MiB.
     */
    void setTranspositionTableSize(unsigned mib);

    /**
     * @brief Clear the transposition table.
     */
    void clearTranspositionTable();

private:
    enum class NodeType
    {
        Root,
        Pv,
        Cut, // Fail low
        All  // Fail high
    };

    Score search(NodeType nodeType, ThreadData &thread, PlyData *ply,
                 Position &position, int16_t depth, Score alpha, Score beta,
                 int color, bool doPruning, std::atomic<bool> &running);

    Score quiescence(NodeType nodeType, ThreadData &thread, PlyData *ply,
                     Position &position, Score alpha, Score beta, int color,
                     std::atomic<bool> &running);

    std::vector<Move> extractPrincipalVariation(Position pos, Score score);

    Evaluator evaluator_;
    MoveGenerator moveGenerator_;

    TimeManager timeManager_;
    TranspositionTable transpositionTable_;

    InfoSignal infoSignal_;

    ThreadPool threadPool_;
};
}

#endif // SEARCHENGINE_H
