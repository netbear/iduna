#ifndef CASTLINGRIGHTS_H
#define CASTLINGRIGHTS_H

#include "enumbitfield.h"

namespace idun
{

enum class CastlingRights
{
    NONE = 0,
    WHITE_KING_SIDE = 1,
    WHITE_QUEEN_SIDE = 2,
    BLACK_KING_SIDE = 4,
    BLACK_QUEEN_SIDE = 8,
    ALL = 15
};

template <> struct enable_enum_bitfield_operators<CastlingRights>
{
    static const bool enable = true;
};
}

#endif // CASTLINGRIGHTS_H
