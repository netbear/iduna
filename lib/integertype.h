#ifndef ARITHMETIC_H
#define ARITHMETIC_H

#include <istream>
#include <ostream>

namespace idun
{

template <typename BaseType, typename Tag> class IntegerType
{
public:
    using base_type = BaseType;
    using tag_type = Tag;

    // Assignment
    constexpr IntegerType() = default;
    constexpr IntegerType(const IntegerType &other) = default;
    constexpr IntegerType(IntegerType &&other) = default;
    constexpr IntegerType &operator=(const IntegerType &other) = default;
    constexpr IntegerType &operator=(IntegerType &&other) = default;

    constexpr explicit IntegerType(const base_type &other) noexcept(
        std::is_nothrow_constructible<base_type>::value)
        : value{other}
    {
    }

    template <class U, class V>
    constexpr explicit IntegerType(const IntegerType<U, V> &other) noexcept(
        std::is_nothrow_constructible<base_type>::value)
        : value(other.value)
    {
    }

    constexpr IntegerType &operator=(const base_type &other) noexcept
    {
        value = other;
        return *this;
    }

    // Conversion
    explicit constexpr operator BaseType() noexcept
    {
        return value;
    }

    explicit constexpr operator const BaseType() const noexcept
    {
        return value;
    }

    template <class U, class V>
    explicit constexpr operator IntegerType<U, V>() noexcept
    {
        return value;
    }

    template <class U, class V>
    explicit constexpr operator const IntegerType<U, V>() const noexcept
    {
        return value;
    }

    template <class T> explicit constexpr operator T() noexcept
    {
        return static_cast<T>(value);
    }

    template <class T> explicit constexpr operator const T() const noexcept
    {
        return static_cast<T>(value);
    }

    explicit constexpr operator bool() noexcept
    {
        return value != 0;
    }

    explicit constexpr operator bool() const noexcept
    {
        return value != 0;
    }

    // Compound assignment
    constexpr IntegerType &operator+=(const IntegerType &other) noexcept
    {
        value += other.value;
        return *this;
    }

    constexpr IntegerType &operator-=(const IntegerType &other) noexcept
    {
        value -= other.value;
        return *this;
    }

    constexpr IntegerType &operator*=(const IntegerType &other) noexcept
    {
        value *= other.value;
        return *this;
    }

    constexpr IntegerType &operator/=(const IntegerType &other) noexcept
    {
        value /= other.value;
        return *this;
    }

    template <typename Integer>
    constexpr IntegerType &operator%=(const Integer &other) noexcept
    {
        value %= other;
        return *this;
    }

    // Compund bitwise operators
    constexpr IntegerType &operator&=(const IntegerType &other) noexcept
    {
        value &= other.value;
        return *this;
    }

    constexpr IntegerType &operator|=(const IntegerType &other) noexcept
    {
        value |= other.value;
        return *this;
    }

    constexpr IntegerType &operator^=(const IntegerType &other) noexcept
    {
        value ^= other.value;
        return *this;
    }

    template <typename Integer>
    constexpr IntegerType &operator<<=(const Integer &n) noexcept
    {
        value <<= n;
        return *this;
    }

    template <typename Integer>
    constexpr IntegerType &operator>>=(const Integer &n) noexcept
    {
        value >>= n;
        return *this;
    }

    // Prefix and postifx increment/decrement operators
    constexpr IntegerType &operator++() noexcept
    {
        ++value;
        return *this;
    }

    constexpr IntegerType &operator--() noexcept
    {
        --value;
        return *this;
    }

    constexpr IntegerType operator++(int)noexcept
    {
        IntegerType tmp(*this);
        ++(*this);
        return tmp;
    }

    constexpr IntegerType operator--(int)noexcept
    {
        IntegerType tmp(*this);
        --(*this);
        return tmp;
    }

    // Value access operators
    constexpr BaseType &operator()() & noexcept
    {
        return value;
    }
    constexpr BaseType operator()() && noexcept
    {
        return value;
    }
    constexpr BaseType operator()() const &noexcept
    {
        return value;
    }

    template <class T, class U>
    friend constexpr IntegerType<T, U>
    operator~(const IntegerType<T, U> &a) noexcept;

private:
    base_type value;
};

template <class T, class U>
constexpr IntegerType<T, U> operator~(const IntegerType<T, U> &a) noexcept
{
    return IntegerType<T, U>(~a.value);
}

template <class T, class U>
constexpr void swap(IntegerType<T, U> &&a, IntegerType<T, U> &&b) noexcept
{
    std::swap(a(), b());
}

// Arithmetic operators +,-,*,/,unary -
template <class T, class U>
constexpr IntegerType<T, U> operator+(IntegerType<T, U> a,
                                      const IntegerType<T, U> &b) noexcept
{
    return a += b;
}

template <class T, class U>
constexpr IntegerType<T, U> operator-(IntegerType<T, U> a,
                                      const IntegerType<T, U> &b) noexcept
{
    return a -= b;
}

template <class T, class U>
constexpr IntegerType<T, U> operator*(IntegerType<T, U> a,
                                      const IntegerType<T, U> &b) noexcept
{
    return a *= b;
}

template <class T, class U>
constexpr IntegerType<T, U> operator/(IntegerType<T, U> a,
                                      const IntegerType<T, U> &b) noexcept
{
    return a /= b;
}

template <class T, class U, typename Integer>
constexpr IntegerType<T, U> operator%(IntegerType<T, U> a,
                                      const Integer &b) noexcept
{
    return a %= b;
}

template <class T, class U>
constexpr IntegerType<T, U> operator-(IntegerType<T, U> const &other) noexcept
{
    return IntegerType<T, U>{-other()};
}

// Bitwise operators
template <class T, class U>
constexpr IntegerType<T, U> operator&(IntegerType<T, U> a,
                                      const IntegerType<T, U> &b) noexcept
{
    return a &= b;
}

template <class T, class U>
constexpr IntegerType<T, U> operator|(IntegerType<T, U> a,
                                      const IntegerType<T, U> &b) noexcept
{
    return a |= b;
}

template <class T, class U>
constexpr IntegerType<T, U> operator^(IntegerType<T, U> a,
                                      const IntegerType<T, U> &b) noexcept
{
    return a ^= b;
}

template <class T, class U, class Integer>
constexpr IntegerType<T, U> operator<<(IntegerType<T, U> a,
                                       const Integer &b) noexcept
{
    return a <<= b;
}

template <class T, class U, class Integer>
constexpr IntegerType<T, U> operator>>(IntegerType<T, U> a,
                                       const Integer &b) noexcept
{
    return a >>= b;
}

// Comparison operators
template <class T, class U>
constexpr bool operator==(const IntegerType<T, U> &a,
                          const IntegerType<T, U> &b) noexcept
{
    return a() == b();
}

template <class T, class U>
constexpr bool operator<(const IntegerType<T, U> &a,
                         const IntegerType<T, U> &b) noexcept
{
    return a() < b();
}

template <class T, class U>
constexpr bool operator<=(const IntegerType<T, U> &a,
                          const IntegerType<T, U> &b) noexcept
{
    return a < b || a == b;
}

template <class T, class U>
constexpr bool operator!=(const IntegerType<T, U> &a,
                          const IntegerType<T, U> &b) noexcept
{
    return !(a == b);
}

template <class T, class U>
constexpr bool operator>(const IntegerType<T, U> &a,
                         const IntegerType<T, U> &b) noexcept
{
    return !(a <= b);
}

template <class T, class U>
constexpr bool operator>=(const IntegerType<T, U> &a,
                          const IntegerType<T, U> &b) noexcept
{
    return !(a < b);
}

template <class C, class CT, class T, class B>
inline auto operator<<(std::basic_ostream<C, CT> &o, const IntegerType<T, B> &v)
    -> decltype(o << v())
{
    return o << v();
}

template <class C, class CT, class T, class B>
inline auto operator>>(std::basic_istream<C, CT> &o, const IntegerType<T, B> &v)
    -> decltype(o << v())
{
    return o >> v();
}
}

#endif // ARITHMETIC_H
