#include <boost/program_options.hpp>
#include <chrono>
#include <iostream>
#include <string>
#include <thread>

#include "universalchessinterface.h"
#include "version.h"

namespace po = boost::program_options;

int main(int argc, char **argv)
{
    std::cout << "iduna " << version() << std::endl;
    po::options_description desc("Allowed options");
    desc.add_options()("help,h", "produce help message")(
        "log", po::value<std::string>(), "log engine input/output to file")(
        "fen", po::value<std::string>(), "set initial position")(
        "moves", po::value<std::string>(), "set initial position")(
        "depth", po::value<int>()->default_value(10),
        "search depth")("go", "start the serach of the initial position to "
                              "given depth and terminate when done");

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if(vm.count("help"))
    {
        std::cout << desc << std::endl;
        return 1;
    }

    UniversalChessInterface uci;

    if(vm.count("log"))
    {
        uci.setLogFile(vm["log"].as<std::string>());
    }

    std::atomic<bool> run(true);

    if(vm.count("go"))
    {
        uci.parseLine("uci");

        uci.parseLine("setoption Hash 512");

        if(vm.count("fen"))
        {
            uci.parseLine("position fen " + vm["fen"].as<std::string>());
        }
        else if(vm.count("moves"))
        {
            uci.parseLine("position startpos moves " +
                          vm["moves"].as<std::string>());
        }

        uci.subscribeCompleted([&run]() { run = false; });
        uci.parseLine("go depth " +
                      boost::lexical_cast<std::string>(vm["depth"].as<int>()));

        while(run)
        {
            using namespace std::chrono_literals;
            std::this_thread::sleep_for(2ms);
        }
    }
    else
    {
        while(run)
        {
            std::string line;
            std::cin >> std::ws;
            std::getline(std::cin, line);

            run = uci.parseLine(line);
        };
    }

    return 0;
}
