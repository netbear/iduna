#include "universalchessinterface.h"

#include <iostream>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include "version.h"
#include <movegenerator.h>
#include <searchoptions.h>

UniversalChessInterface::UniversalChessInterface()
    : debug_(false), logToFile_(false)
{
    engine_.subscribeInfo([this](auto pv) { this->sendInfo(pv); });

    // Set a known state.
    position_.set("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");

    threadPool_.start(1);
}

UniversalChessInterface::~UniversalChessInterface()
{
    threadPool_.stop();
}

boost::signals2::connection UniversalChessInterface::subscribeCompleted(
    const CompletedSignal::slot_type &subscriber)
{
    return completedSignal_.connect(subscriber);
}

bool UniversalChessInterface::parseLine(const std::string &line)
{
    log("GUI: " + line + '\n');

    bool result = true;

    if(line.empty())
    {
        return result;
    }

    std::istringstream stream(line);

    std::string cmd;
    stream >> cmd;

    if(cmd == "uci")
    {
        sendId();
        sendOptions();
        sendUciOk();
    }
    else if(cmd == "isready")
    {
        sendReadyOk();
    }
    else if(cmd == "debug")
    {
        std::string state;
        stream >> state;
        if(state == "on")
        {
            debug_ = true;
        }
        else
        {
            debug_ = false;
        }
    }
    else if(cmd == "setoption")
    {
        handleSetOption(stream);
    }
    else if(cmd == "register")
    {
        // Not used
    }
    else if(cmd == "ucinewgame")
    {
        engine_.newGame();
    }
    else if(cmd == "position")
    {
        handlePosition(stream);
    }
    else if(cmd == "go")
    {
        handleGo(stream);
    }
    else if(cmd == "stop")
    {
        handleStop();
    }
    else if(cmd == "ponderhit")
    {
        // Not implemented
        debug("Ponder hit not implemented.");
    }
    else if(cmd == "quit")
    {
        result = false;
    }

    return result;
}

bool UniversalChessInterface::setLogFile(const std::string &logFile)
{
    log_ = std::ofstream(logFile);
    logToFile_ = log_.is_open();
    return logToFile_;
}

void UniversalChessInterface::sendId()
{
    std::ostringstream out;
    out << "id name iduna " << version() << "\n";
    out << "id author Bjorn Samvik\n";
    send(out.str());
}

void UniversalChessInterface::sendOptions()
{
    std::ostringstream out;
    out << "option name Hash type spin default 1 min 1 max 1048576\n";
    out << "option name Log File type string default\n";
    out << "option name Clear Hash type button\n";
    send(out.str());
}

void UniversalChessInterface::sendUciOk()
{
    std::ostringstream out;
    out << "uciok\n";
    send(out.str());
}

void UniversalChessInterface::sendReadyOk()
{
    std::ostringstream out;
    out << "readyok\n";
    send(out.str());
}

void UniversalChessInterface::sendBestMove(idun::Move bestMove,
                                           idun::Move ponder)
{
    std::ostringstream out;
    out << "bestmove " << bestMove;
    if(ponder.isValid())
    {
        out << "ponder " << ponder;
    }
    out << '\n';
    send(out.str());
}

void UniversalChessInterface::sendInfo(
    idun::SearchEngine::PrincipalVariation pv)
{

    std::string scoreType;
    switch(pv.scoreType)
    {
        case idun::SearchEngine::PrincipalVariation::ScoreType::CentiPawns:
            scoreType = "cp";
            break;
        case idun::SearchEngine::PrincipalVariation::ScoreType::Mate:
            scoreType = "mate";
            break;
        case idun::SearchEngine::PrincipalVariation::ScoreType::LowerBound:
            scoreType = "lowerbound";
            break;
        case idun::SearchEngine::PrincipalVariation::ScoreType::UpperBound:
            scoreType = "upperbound";
            break;
    }

    std::ostringstream out;
    out << "info"
        << " depth " << pv.depth << " seldepth " << pv.selectiveDepth
        << " time " << pv.time << " score " << scoreType << " " << pv.score
        << " nodes " << pv.nodes << " nps " << pv.nodesPerSecond << " pv";

    for(const auto &move : pv.moves)
    {
        out << " " << move;
    }

    out << '\n';
    send(out.str());
}

void UniversalChessInterface::handleSetOption(std::istringstream &stream)
{
    // Parse option
    std::string line = stream.str();

    auto nameBegin = line.find("name") + 4;
    auto nameEnd = line.find("value");

    std::string name, value;
    if(nameEnd == std::string::npos)
    {
        name = line.substr(nameBegin);
    }
    else
    {
        auto valueBegin = nameEnd + 5;
        name = line.substr(nameBegin, nameEnd - nameBegin);
        value = line.substr(valueBegin);
        boost::trim(value);
    }

    boost::trim(name);

    // Handle the option
    if(name == "Hash")
    {
        unsigned mib = boost::lexical_cast<unsigned>(value);
        engine_.setTranspositionTableSize(mib);
    }
    else if(name == "Log File")
    {
        if(!value.empty())
        {
            setLogFile(value);
        }
    }
    else if(name == "Clear Hash")
    {
        engine_.clearTranspositionTable();
    }
}

void UniversalChessInterface::handlePosition(std::istringstream &stream)
{
    std::string type;

    bool moves = false;
    while(!stream.eof())
    {
        stream >> type;

        if(moves)
        {
            idun::MoveGenerator mg;
            idun::Move move = mg.getMove(position_, type);
            if(move.isValid())
            {
                idun::MoveInformation mi;
                position_.doMove(move, &mi);
            }
            else
            {
                debug("Error: Invalid move received from GUI.");
                break;
            }
        }
        else if(type == "startpos")
        {
            position_.set(
                "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
        }
        else if(type == "fen")
        {
            std::string fen;

            // Get all 6 parts of the fen.
            // TODO: let position_.set() take a stream.
            std::string part;
            for(int i = 0; i < 6; ++i)
            {
                stream >> part;
                fen += part + ' ';
            }

            position_.set(fen);
        }
        else if(type == "moves")
        {
            moves = true;
        }
    }

    completedSignal_();
}

void UniversalChessInterface::handleGo(std::istringstream &stream)
{
    idun::SearchOptions searchOptions;

    while(!stream.eof())
    {
        std::string cmd;
        stream >> cmd;
        if(cmd == "searchmoves")
        {

            std::vector<std::string> moves;
            while(!stream.eof())
            {
                std::string move;
                stream >> move;
                moves.push_back(move);
            }
            searchOptions.setSearchMoves(moves);
        }
        else if(cmd == "ponder")
        {
            searchOptions.setPonderSearch(true);
        }
        else if(cmd == "wtime") // White has x ms left on the clock
        {
            std::string tmp;
            stream >> tmp;
            searchOptions.setWhiteTimeLeft(boost::lexical_cast<int>(tmp));
        }
        else if(cmd == "btime") // Black has x ms left on the clock
        {
            std::string tmp;
            stream >> tmp;
            searchOptions.setBlackTimeLeft(boost::lexical_cast<int>(tmp));
        }
        else if(cmd == "winc") // White increment per move in ms
        {
            std::string tmp;
            stream >> tmp;
            searchOptions.setWhiteIncrement(boost::lexical_cast<int>(tmp));
        }
        else if(cmd == "binc") // Black increment per move in ms
        {
            std::string tmp;
            stream >> tmp;
            searchOptions.setBlackIncrement(boost::lexical_cast<int>(tmp));
        }
        else if(cmd == "movestogo") // X moves until next time control.
        {
            std::string tmp;
            stream >> tmp;
            searchOptions.setMovesToGo(boost::lexical_cast<int>(tmp));
        }
        else if(cmd == "depth") // Search x plies only
        {
            std::string tmp;
            stream >> tmp;
            searchOptions.setDepthLimit(boost::lexical_cast<int>(tmp));
        }
        else if(cmd == "nodes") // Search x nodes only
        {
            std::string tmp;
            stream >> tmp;
            searchOptions.setNodeLimit(boost::lexical_cast<int>(tmp));
        }
        else if(cmd == "mate") // Search for mate in x moves
        {
            searchOptions.setMateSearch(true);
        }
        else if(cmd == "movetime") // Search exact x ms
        {
            std::string tmp;
            stream >> tmp;
            searchOptions.setTimeLimit(boost::lexical_cast<int>(tmp));
        }
        else if(cmd == "infinite") // Search until stop command
        {
            searchOptions.setInfiniteSearch(true);
        }
    }

    run_ = true;
    threadPool_.enqueue([this, searchOptions]() {
        idun::Move move = engine_.run(position_, searchOptions, run_);
        sendBestMove(move);

        completedSignal_();
    });
}

void UniversalChessInterface::handleStop()
{
    run_ = false;
    completedSignal_();
}

void UniversalChessInterface::send(std::string data)
{
    log("Engine: " + data);
    std::cout << data << std::flush;
}

void UniversalChessInterface::debug(std::string data)
{
    if(debug_)
    {
        send("info string " + data);
    }
}

void UniversalChessInterface::log(std::string data)
{
    if(logToFile_)
    {
        log_ << data << std::flush;
    }
}
