#ifndef VERSION_H_IN
#define VERSION_H_IN

#include <string>

std::string gitHash();
std::string gitShortHash();
std::string gitRefSpec();
std::string version();

#endif // VERSION_H_IN
