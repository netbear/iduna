#ifndef UNIVERSALCHESSINTERFACE_H
#define UNIVERSALCHESSINTERFACE_H

#include <atomic>
#include <fstream>
#include <sstream>
#include <string>
#include <thread>

#include <boost/asio/io_service.hpp>
#include <boost/signals2.hpp>
#include <boost/thread/thread.hpp>

#include <position.h>
#include <searchengine.h>

#include "threadpool.h"

class UniversalChessInterface
{
public:
    UniversalChessInterface();
    ~UniversalChessInterface();

    typedef boost::signals2::signal<void()> CompletedSignal;
    boost::signals2::connection
    subscribeCompleted(const CompletedSignal::slot_type &subscriber);

    bool parseLine(const std::string &line);

    bool setLogFile(const std::string &logFile);

private:
    void sendId();
    void sendOptions();
    void sendUciOk();
    void sendReadyOk();
    void sendBestMove(idun::Move bestMove, idun::Move ponder = idun::Move());
    void sendInfo(idun::SearchEngine::PrincipalVariation pv);

    void handleSetOption(std::istringstream &stream);
    void handlePosition(std::istringstream &stream);
    void handleGo(std::istringstream &stream);
    void handleStop();

    void send(std::string data);
    void debug(std::string data);
    void log(std::string data);

    std::thread searchThread_;
    std::atomic<bool> run_;

    bool debug_;

    idun::Position position_;
    idun::SearchEngine engine_;

    idun::ThreadPool threadPool_;

    CompletedSignal completedSignal_;

    bool logToFile_;
    std::ofstream log_;
};

#endif // UNIVERSALCHESSINTERFACE_H
